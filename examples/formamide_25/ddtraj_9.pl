
 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.000000    0.401721   -0.096031
    2          7             0        0.000000   -0.162504    1.145371
    3          1             0        0.000000    1.541283   -0.009698
    4          1             0        0.000000    0.374796    1.979555
    5          8             0        0.000000   -0.207339   -1.131411
    6          1             0        0.000000   -1.153754    1.217289
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.002720    0.404748   -0.082115
    2          7             0       -0.010899   -0.168136    1.137945
    3          1             0       -0.013143    1.477899   -0.041954
    4          1             0        0.018267    0.461925    1.979676
    5          8             0        0.007005   -0.201344   -1.135986
    6          1             0        0.002759   -1.230430    1.259512
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.004726    0.432046   -0.063574
    2          7             0       -0.031240   -0.188051    1.123541
    3          1             0       -0.049224    1.474052   -0.106073
    4          1             0        0.112639    0.526298    2.038032
    5          8             0        0.015301   -0.215618   -1.140074
    6          1             0        0.184092   -1.112760    1.309533
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.012317    0.480079   -0.026343
    2          7             0       -0.037088   -0.200913    1.123797
    3          1             0       -0.069677    1.518724   -0.200292
    4          1             0        0.163522    0.447705    1.901286
    5          8             0        0.018168   -0.239629   -1.155395
    6          1             0        0.279804   -1.090979    1.336794
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.021210    0.527362    0.001177
    2          7             0       -0.037823   -0.204639    1.120346
    3          1             0       -0.110197    1.602986   -0.289383
    4          1             0        0.216230    0.373427    1.850031
    5          8             0        0.018248   -0.265445   -1.164963
    6          1             0        0.382442   -1.202428    1.349267
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.028181    0.565372    0.003962
    2          7             0       -0.039052   -0.209933    1.113802
    3          1             0       -0.188011    1.656172   -0.341293
    4          1             0        0.283901    0.384532    1.941381
    5          8             0        0.019297   -0.286174   -1.162380
    6          1             0        0.476012   -1.316767    1.326581
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.035979    0.595683   -0.013649
    2          7             0       -0.042066   -0.207814    1.115400
    3          1             0       -0.268422    1.627070   -0.337042
    4          1             0        0.348995    0.399510    2.041877
    5          8             0        0.022806   -0.302794   -1.153119
    6          1             0        0.570377   -1.429203    1.262356
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.044386    0.610404   -0.038837
    2          7             0       -0.047453   -0.198182    1.131273
    3          1             0       -0.321151    1.584507   -0.284090
    4          1             0        0.381824    0.354262    2.030462
    5          8             0        0.028857   -0.314434   -1.144954
    6          1             0        0.669185   -1.465770    1.170609
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.053309    0.602339   -0.062474
    2          7             0       -0.055365   -0.183904    1.158775
    3          1             0       -0.340017    1.618173   -0.197760
    4          1             0        0.374615    0.250862    1.883992
    5          8             0        0.037942   -0.319068   -1.141612
    6          1             0        0.767242   -1.424827    1.077006
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.062984    0.581035   -0.083764
    2          7             0       -0.067186   -0.169484    1.182324
    3          1             0       -0.357230    1.697735   -0.113348
    4          1             0        0.363657    0.179807    1.752999
    5          8             0        0.049603   -0.318821   -1.138769
    6          1             0        0.889775   -1.383953    1.004758
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.076291    0.559033   -0.107086
    2          7             0       -0.081295   -0.162458    1.182523
    3          1             0       -0.399812    1.729327   -0.048623
    4          1             0        0.400063    0.278629    1.829201
    5          8             0        0.063109   -0.314636   -1.129562
    6          1             0        1.036099   -1.416432    0.992638
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.087496    0.539620   -0.117181
    2          7             0       -0.100109   -0.157230    1.153417
    3          1             0       -0.418669    1.669749   -0.051447
    4          1             0        0.480165    0.534423    2.111793
    5          8             0        0.072109   -0.304738   -1.118675
    6          1             0        1.226854   -1.611239    1.064720
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.091321    0.519236   -0.098413
    2          7             0       -0.124756   -0.141870    1.121273
    3          1             0       -0.347127    1.552639   -0.128230
    4          1             0        0.555209    0.696075    2.313035
    5          8             0        0.073317   -0.292352   -1.119044
    6          1             0        1.449105   -1.823057    1.169291
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.091753    0.514492   -0.093933
    2          7             0       -0.128703   -0.137764    1.117284
    3          1             0       -0.327307    1.539261   -0.139626
    4          1             0        0.559331    0.707248    2.325935
    5          8             0        0.073277   -0.289949   -1.120177
    6          1             0        1.485796   -1.859550    1.187867
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.091753    0.514492   -0.093933
    2          7             0       -0.128703   -0.137764    1.117284
    3          1             0       -0.327307    1.539261   -0.139626
    4          1             0        0.559331    0.707248    2.325935
    5          8             0        0.073277   -0.289949   -1.120177
    6          1             0        1.485796   -1.859550    1.187867
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.091753    0.514492   -0.093933
    2          7             0       -0.128703   -0.137764    1.117284
    3          1             0       -0.327307    1.539261   -0.139626
    4          1             0        0.559331    0.707248    2.325935
    5          8             0        0.073277   -0.289949   -1.120177
    6          1             0        1.485796   -1.859550    1.187867
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.091753    0.514492   -0.093933
    2          7             0       -0.128703   -0.137764    1.117284
    3          1             0       -0.327307    1.539261   -0.139626
    4          1             0        0.559331    0.707248    2.325935
    5          8             0        0.073277   -0.289949   -1.120177
    6          1             0        1.485796   -1.859550    1.187867
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.091753    0.514492   -0.093933
    2          7             0       -0.128703   -0.137764    1.117284
    3          1             0       -0.327307    1.539261   -0.139626
    4          1             0        0.559331    0.707248    2.325935
    5          8             0        0.073277   -0.289949   -1.120177
    6          1             0        1.485796   -1.859550    1.187867
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.091753    0.514492   -0.093933
    2          7             0       -0.128703   -0.137764    1.117284
    3          1             0       -0.327307    1.539261   -0.139626
    4          1             0        0.559331    0.707248    2.325935
    5          8             0        0.073277   -0.289949   -1.120177
    6          1             0        1.485796   -1.859550    1.187867
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.091753    0.514492   -0.093933
    2          7             0       -0.128703   -0.137764    1.117284
    3          1             0       -0.327307    1.539261   -0.139626
    4          1             0        0.559331    0.707248    2.325935
    5          8             0        0.073277   -0.289949   -1.120177
    6          1             0        1.485796   -1.859550    1.187867
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.091753    0.514492   -0.093933
    2          7             0       -0.128703   -0.137764    1.117284
    3          1             0       -0.327307    1.539261   -0.139626
    4          1             0        0.559331    0.707248    2.325935
    5          8             0        0.073277   -0.289949   -1.120177
    6          1             0        1.485796   -1.859550    1.187867
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.091753    0.514492   -0.093933
    2          7             0       -0.128703   -0.137764    1.117284
    3          1             0       -0.327307    1.539261   -0.139626
    4          1             0        0.559331    0.707248    2.325935
    5          8             0        0.073277   -0.289949   -1.120177
    6          1             0        1.485796   -1.859550    1.187867
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.091753    0.514492   -0.093933
    2          7             0       -0.128703   -0.137764    1.117284
    3          1             0       -0.327307    1.539261   -0.139626
    4          1             0        0.559331    0.707248    2.325935
    5          8             0        0.073277   -0.289949   -1.120177
    6          1             0        1.485796   -1.859550    1.187867
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.091753    0.514492   -0.093933
    2          7             0       -0.128703   -0.137764    1.117284
    3          1             0       -0.327307    1.539261   -0.139626
    4          1             0        0.559331    0.707248    2.325935
    5          8             0        0.073277   -0.289949   -1.120177
    6          1             0        1.485796   -1.859550    1.187867
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.091753    0.514492   -0.093933
    2          7             0       -0.128703   -0.137764    1.117284
    3          1             0       -0.327307    1.539261   -0.139626
    4          1             0        0.559331    0.707248    2.325935
    5          8             0        0.073277   -0.289949   -1.120177
    6          1             0        1.485796   -1.859550    1.187867
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.091753    0.514492   -0.093933
    2          7             0       -0.128703   -0.137764    1.117284
    3          1             0       -0.327307    1.539261   -0.139626
    4          1             0        0.559331    0.707248    2.325935
    5          8             0        0.073277   -0.289949   -1.120177
    6          1             0        1.485796   -1.859550    1.187867
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.091753    0.514492   -0.093933
    2          7             0       -0.128703   -0.137764    1.117284
    3          1             0       -0.327307    1.539261   -0.139626
    4          1             0        0.559331    0.707248    2.325935
    5          8             0        0.073277   -0.289949   -1.120177
    6          1             0        1.485796   -1.859550    1.187867
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.091753    0.514492   -0.093933
    2          7             0       -0.128703   -0.137764    1.117284
    3          1             0       -0.327307    1.539261   -0.139626
    4          1             0        0.559331    0.707248    2.325935
    5          8             0        0.073277   -0.289949   -1.120177
    6          1             0        1.485796   -1.859550    1.187867
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.091753    0.514492   -0.093933
    2          7             0       -0.128703   -0.137764    1.117284
    3          1             0       -0.327307    1.539261   -0.139626
    4          1             0        0.559331    0.707248    2.325935
    5          8             0        0.073277   -0.289949   -1.120177
    6          1             0        1.485796   -1.859550    1.187867
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.091753    0.514492   -0.093933
    2          7             0       -0.128703   -0.137764    1.117284
    3          1             0       -0.327307    1.539261   -0.139626
    4          1             0        0.559331    0.707248    2.325935
    5          8             0        0.073277   -0.289949   -1.120177
    6          1             0        1.485796   -1.859550    1.187867
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.091753    0.514492   -0.093933
    2          7             0       -0.128703   -0.137764    1.117284
    3          1             0       -0.327307    1.539261   -0.139626
    4          1             0        0.559331    0.707248    2.325935
    5          8             0        0.073277   -0.289949   -1.120177
    6          1             0        1.485796   -1.859550    1.187867
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.091753    0.514492   -0.093933
    2          7             0       -0.128703   -0.137764    1.117284
    3          1             0       -0.327307    1.539261   -0.139626
    4          1             0        0.559331    0.707248    2.325935
    5          8             0        0.073277   -0.289949   -1.120177
    6          1             0        1.485796   -1.859550    1.187867
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.091753    0.514492   -0.093933
    2          7             0       -0.128703   -0.137764    1.117284
    3          1             0       -0.327307    1.539261   -0.139626
    4          1             0        0.559331    0.707248    2.325935
    5          8             0        0.073277   -0.289949   -1.120177
    6          1             0        1.485796   -1.859550    1.187867
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.091753    0.514492   -0.093933
    2          7             0       -0.128703   -0.137764    1.117284
    3          1             0       -0.327307    1.539261   -0.139626
    4          1             0        0.559331    0.707248    2.325935
    5          8             0        0.073277   -0.289949   -1.120177
    6          1             0        1.485796   -1.859550    1.187867
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.091753    0.514492   -0.093933
    2          7             0       -0.128703   -0.137764    1.117284
    3          1             0       -0.327307    1.539261   -0.139626
    4          1             0        0.559331    0.707248    2.325935
    5          8             0        0.073277   -0.289949   -1.120177
    6          1             0        1.485796   -1.859550    1.187867
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.091753    0.514492   -0.093933
    2          7             0       -0.128703   -0.137764    1.117284
    3          1             0       -0.327307    1.539261   -0.139626
    4          1             0        0.559331    0.707248    2.325935
    5          8             0        0.073277   -0.289949   -1.120177
    6          1             0        1.485796   -1.859550    1.187867
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.091753    0.514492   -0.093933
    2          7             0       -0.128703   -0.137764    1.117284
    3          1             0       -0.327307    1.539261   -0.139626
    4          1             0        0.559331    0.707248    2.325935
    5          8             0        0.073277   -0.289949   -1.120177
    6          1             0        1.485796   -1.859550    1.187867
 ---------------------------------------------------------------------
