
 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.679721   -0.020178    0.000000
    2          8             0       -0.757223    0.122741    0.000000
    3          1             0        1.082683    0.987864    0.000000
    4          1             0        1.024117   -0.545246   -0.892224
    5          1             0        1.024117   -0.545246    0.892224
    6          1             0       -1.151456   -0.758236    0.000000
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.671361   -0.017894   -0.000849
    2          8             0       -0.747601    0.123669   -0.000944
    3          1             0        1.050575    0.970607    0.030764
    4          1             0        0.996655   -0.566929   -0.894652
    5          1             0        1.033616   -0.530941    0.889475
    6          1             0       -1.154167   -0.775838   -0.000660
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.661566   -0.018997   -0.008686
    2          8             0       -0.741919    0.129431    0.000117
    3          1             0        1.031769    0.997382    0.102130
    4          1             0        1.016183   -0.630312   -0.876987
    5          1             0        1.062237   -0.513660    0.877748
    6          1             0       -1.156676   -0.834419   -0.001629
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.660159   -0.012674   -0.022136
    2          8             0       -0.742095    0.137616    0.003313
    3          1             0        1.050591    1.011073    0.146315
    4          1             0        1.008635   -0.689034   -0.833737
    5          1             0        1.018406   -0.552581    0.899573
    6          1             0       -1.104226   -0.955702   -0.001722
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.653152   -0.007987   -0.022791
    2          8             0       -0.741009    0.146024    0.005681
    3          1             0        1.081288    1.018876    0.110782
    4          1             0        1.036316   -0.708715   -0.782384
    5          1             0        0.970314   -0.610292    0.853371
    6          1             0       -1.048612   -1.075240   -0.000750
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.659036   -0.008136   -0.018221
    2          8             0       -0.744211    0.157256    0.005364
    3          1             0        1.117033    1.020594    0.081114
    4          1             0        1.006751   -0.701095   -0.791850
    5          1             0        0.927161   -0.625622    0.842923
    6          1             0       -1.030555   -1.245940   -0.000351
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.661351   -0.008930   -0.014030
    2          8             0       -0.737247    0.162350    0.002242
    3          1             0        1.129410    0.992462    0.059735
    4          1             0        0.913298   -0.641500   -0.837173
    5          1             0        0.911460   -0.610127    0.908925
    6          1             0       -1.070837   -1.365275   -0.000493
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.662652    0.001642   -0.015582
    2          8             0       -0.733550    0.166587    0.001771
    3          1             0        1.180841    0.962010    0.045786
    4          1             0        0.823610   -0.637737   -0.871123
    5          1             0        0.842123   -0.646658    0.982245
    6          1             0       -1.036596   -1.496432   -0.000290
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.665728    0.007328   -0.007209
    2          8             0       -0.734269    0.173420    0.001816
    3          1             0        1.251843    0.944539    0.009972
    4          1             0        0.794157   -0.638274   -0.919198
    5          1             0        0.770003   -0.650053    0.965869
    6          1             0       -1.030970   -1.651827    0.000205
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.666403    0.006238    0.005938
    2          8             0       -0.735115    0.181283    0.001879
    3          1             0        1.323842    0.927395   -0.037757
    4          1             0        0.812284   -0.614018   -0.943885
    5          1             0        0.722240   -0.615718    0.881133
    6          1             0       -1.068307   -1.805220    0.000832
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.662428    0.001184    0.012578
    2          8             0       -0.734432    0.190721    0.002413
    3          1             0        1.380429    0.897824   -0.077590
    4          1             0        0.857356   -0.559754   -0.912883
    5          1             0        0.716481   -0.577560    0.802336
    6          1             0       -1.128384   -1.957517    0.001497
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.661425   -0.002536    0.008462
    2          8             0       -0.737874    0.201191    0.003264
    3          1             0        1.397672    0.859760   -0.088199
    4          1             0        0.914794   -0.518733   -0.865515
    5          1             0        0.764247   -0.573473    0.800473
    6          1             0       -1.184399   -2.086108    0.001873
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.664213   -0.002366   -0.003767
    2          8             0       -0.744788    0.212148    0.004367
    3          1             0        1.374007    0.843104   -0.057925
    4          1             0        0.976866   -0.535396   -0.855531
    5          1             0        0.849324   -0.610230    0.887760
    6          1             0       -1.230299   -2.191476    0.001704
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.671436   -0.003509   -0.014987
    2          8             0       -0.752112    0.223788    0.005076
    3          1             0        1.312946    0.873593    0.001841
    4          1             0        1.028491   -0.605265   -0.885510
    5          1             0        0.935078   -0.647040    0.980482
    6          1             0       -1.274512   -2.285579    0.000944
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.684649   -0.012499   -0.018718
    2          8             0       -0.758299    0.234533    0.005080
    3          1             0        1.236284    0.942512    0.068999
    4          1             0        1.049043   -0.676602   -0.896093
    5          1             0        0.979366   -0.628747    0.969352
    6          1             0       -1.320542   -2.363737   -0.000171
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.705648   -0.026724   -0.017297
    2          8             0       -0.765951    0.242813    0.005027
    3          1             0        1.168782    1.000438    0.123662
    4          1             0        1.040754   -0.708472   -0.841389
    5          1             0        0.972255   -0.557314    0.845279
    6          1             0       -1.366327   -2.421893   -0.001160
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.729666   -0.035461   -0.015000
    2          8             0       -0.774772    0.249223    0.005598
    3          1             0        1.105917    1.000666    0.146840
    4          1             0        1.029771   -0.712604   -0.792450
    5          1             0        0.935427   -0.498930    0.737592
    6          1             0       -1.401907   -2.473342   -0.001523
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.749397   -0.032518   -0.015792
    2          8             0       -0.783802    0.251503    0.007138
    3          1             0        1.076538    0.953444    0.134582
    4          1             0        1.034572   -0.705002   -0.810178
    5          1             0        0.893736   -0.499934    0.752371
    6          1             0       -1.426825   -2.504152   -0.001074
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.762854   -0.025794   -0.019674
    2          8             0       -0.794911    0.250496    0.008837
    3          1             0        1.108888    0.931853    0.092756
    4          1             0        1.048948   -0.683085   -0.891494
    5          1             0        0.867216   -0.557120    0.893582
    6          1             0       -1.429901   -2.511997   -0.000033
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.772445   -0.028228   -0.023594
    2          8             0       -0.807805    0.248584    0.009650
    3          1             0        1.206503    0.991523    0.034955
    4          1             0        1.047157   -0.624240   -0.964198
    5          1             0        0.851024   -0.615472    1.056212
    6          1             0       -1.418401   -2.513100    0.001162
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.782208   -0.035413   -0.022558
    2          8             0       -0.821366    0.247441    0.009870
    3          1             0        1.324337    1.034403   -0.023356
    4          1             0        1.035816   -0.541890   -0.970209
    5          1             0        0.821848   -0.623662    1.103546
    6          1             0       -1.397533   -2.526493    0.002198
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.787326   -0.031977   -0.013289
    2          8             0       -0.830067    0.246804    0.009628
    3          1             0        1.402200    0.934510   -0.057612
    4          1             0        1.019611   -0.497818   -0.942150
    5          1             0        0.768196   -0.576877    1.003456
    6          1             0       -1.330520   -2.549075    0.002610
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.779402   -0.016723   -0.002107
    2          8             0       -0.829024    0.245062    0.008440
    3          1             0        1.440843    0.762523   -0.053656
    4          1             0        0.993024   -0.518904   -0.934481
    5          1             0        0.729660   -0.527268    0.878752
    6          1             0       -1.228272   -2.561084    0.002184
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.760356    0.000100    0.000370
    2          8             0       -0.821877    0.243265    0.006374
    3          1             0        1.459141    0.678176   -0.012162
    4          1             0        0.951693   -0.598829   -0.944454
    5          1             0        0.747058   -0.542715    0.851505
    6          1             0       -1.109982   -2.554230    0.001120
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.735119    0.005164   -0.006272
    2          8             0       -0.811975    0.243426    0.003557
    3          1             0        1.471548    0.749706    0.041533
    4          1             0        0.896179   -0.681317   -0.923087
    5          1             0        0.816103   -0.611932    0.900388
    6          1             0       -0.992351   -2.536855   -0.000114
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.735119    0.005164   -0.006272
    2          8             0       -0.811975    0.243426    0.003557
    3          1             0        1.471548    0.749706    0.041533
    4          1             0        0.896179   -0.681317   -0.923087
    5          1             0        0.816103   -0.611932    0.900388
    6          1             0       -0.992351   -2.536855   -0.000114
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.735119    0.005164   -0.006272
    2          8             0       -0.811975    0.243426    0.003557
    3          1             0        1.471548    0.749706    0.041533
    4          1             0        0.896179   -0.681317   -0.923087
    5          1             0        0.816103   -0.611932    0.900388
    6          1             0       -0.992351   -2.536855   -0.000114
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.735119    0.005164   -0.006272
    2          8             0       -0.811975    0.243426    0.003557
    3          1             0        1.471548    0.749706    0.041533
    4          1             0        0.896179   -0.681317   -0.923087
    5          1             0        0.816103   -0.611932    0.900388
    6          1             0       -0.992351   -2.536855   -0.000114
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.735119    0.005164   -0.006272
    2          8             0       -0.811975    0.243426    0.003557
    3          1             0        1.471548    0.749706    0.041533
    4          1             0        0.896179   -0.681317   -0.923087
    5          1             0        0.816103   -0.611932    0.900388
    6          1             0       -0.992351   -2.536855   -0.000114
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.735119    0.005164   -0.006272
    2          8             0       -0.811975    0.243426    0.003557
    3          1             0        1.471548    0.749706    0.041533
    4          1             0        0.896179   -0.681317   -0.923087
    5          1             0        0.816103   -0.611932    0.900388
    6          1             0       -0.992351   -2.536855   -0.000114
 ---------------------------------------------------------------------
