"""Functions to perform tests on IO
"""

import IO.io_funcs as iof


class TestIO:
    """Class to test the io_funcs.py"""

    def test_load_xyz(self):
        """Test that the xyz file is loaded correctly"""
        filename = "./tests/ddtraj_1.pl"
        tnums, __ = iof.load_xyz(filename)
        assert tnums == 101

    # def test_two(self):
    #     x = "hello"
    #     assert hasattr(x, "check")
