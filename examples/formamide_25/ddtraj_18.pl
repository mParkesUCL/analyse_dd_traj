
 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.000000    0.401721   -0.096031
    2          7             0        0.000000   -0.162504    1.145371
    3          1             0        0.000000    1.541283   -0.009698
    4          1             0        0.000000    0.374796    1.979555
    5          8             0        0.000000   -0.207339   -1.131411
    6          1             0        0.000000   -1.153754    1.217289
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.002159    0.421658   -0.071086
    2          7             0        0.004214   -0.167914    1.125009
    3          1             0        0.000413    1.488677   -0.099269
    4          1             0        0.002903    0.394292    2.069194
    5          8             0       -0.004567   -0.216371   -1.136549
    6          1             0        0.036338   -1.139522    1.284671
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.005432    0.448470   -0.088003
    2          7             0        0.013102   -0.166373    1.124661
    3          1             0        0.033472    1.483269    0.004255
    4          1             0       -0.086022    0.332313    2.060236
    5          8             0       -0.009150   -0.228894   -1.133033
    6          1             0        0.080402   -1.214022    1.340574
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.007936    0.489002   -0.055771
    2          7             0        0.017553   -0.182888    1.136328
    3          1             0        0.030079    1.502437   -0.105722
    4          1             0       -0.136948    0.310795    1.892535
    5          8             0       -0.008357   -0.244318   -1.146678
    6          1             0        0.090090   -1.220013    1.288897
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.011949    0.525106   -0.026684
    2          7             0        0.026377   -0.212613    1.134087
    3          1             0        0.030184    1.589134   -0.264620
    4          1             0       -0.181603    0.383208    1.905718
    5          8             0       -0.009570   -0.257208   -1.153320
    6          1             0        0.079085   -1.191438    1.224831
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.016090    0.554172   -0.001824
    2          7             0        0.029120   -0.243897    1.129810
    3          1             0        0.041911    1.649602   -0.429199
    4          1             0       -0.197861    0.491964    1.968689
    5          8             0       -0.006378   -0.263042   -1.156929
    6          1             0        0.044162   -1.179492    1.147163
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.020957    0.606664    0.007745
    2          7             0        0.043319   -0.239931    1.125484
    3          1             0        0.003561    1.654124   -0.448089
    4          1             0       -0.193682    0.278613    1.959144
    5          8             0       -0.009454   -0.289735   -1.160400
    6          1             0       -0.012201   -1.227117    1.176863
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.021505    0.638552    0.004608
    2          7             0        0.060046   -0.230115    1.121119
    3          1             0       -0.029618    1.641888   -0.432178
    4          1             0       -0.230125    0.076459    1.992741
    5          8             0       -0.012290   -0.305889   -1.158804
    6          1             0       -0.123468   -1.272417    1.200019
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.022891    0.673886   -0.039061
    2          7             0        0.080493   -0.232167    1.131429
    3          1             0       -0.065823    1.635261   -0.368493
    4          1             0       -0.245041   -0.061527    2.131999
    5          8             0       -0.020302   -0.325425   -1.140009
    6          1             0       -0.212806   -1.209929    1.075502
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.026247    0.674531   -0.098567
    2          7             0        0.096413   -0.236969    1.152565
    3          1             0       -0.142287    1.703888   -0.255648
    4          1             0       -0.230001   -0.015332    2.238891
    5          8             0       -0.024681   -0.331542   -1.117139
    6          1             0       -0.263115   -1.168645    0.907683
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.026540    0.659190   -0.148276
    2          7             0        0.109656   -0.226608    1.179924
    3          1             0       -0.279890    1.778420   -0.135397
    4          1             0       -0.187718    0.026904    2.226356
    5          8             0       -0.028004   -0.334428   -1.101233
    6          1             0       -0.295587   -1.200905    0.759258
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.024375    0.642152   -0.172399
    2          7             0        0.118448   -0.205649    1.200969
    3          1             0       -0.449087    1.748505   -0.013238
    4          1             0       -0.114244    0.027824    2.087213
    5          8             0       -0.031844   -0.339407   -1.098544
    6          1             0       -0.286880   -1.181242    0.728378
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.020482    0.628043   -0.180227
    2          7             0        0.125490   -0.189618    1.203377
    3          1             0       -0.595183    1.652103    0.057464
    4          1             0       -0.054202    0.046768    2.037018
    5          8             0       -0.037021   -0.343723   -1.098679
    6          1             0       -0.262859   -1.090033    0.769737
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.015400    0.609083   -0.187390
    2          7             0        0.140457   -0.172133    1.194896
    3          1             0       -0.751304    1.529255    0.074491
    4          1             0       -0.067671    0.130244    2.126060
    5          8             0       -0.042167   -0.342859   -1.094186
    6          1             0       -0.280100   -1.081578    0.795496
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.013874    0.582898   -0.192179
    2          7             0        0.163157   -0.153304    1.186290
    3          1             0       -0.836740    1.376647    0.069032
    4          1             0       -0.131748    0.246507    2.227172
    5          8             0       -0.048933   -0.333967   -1.089699
    6          1             0       -0.356757   -1.136197    0.805241
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.019574    0.544241   -0.190757
    2          7             0        0.184893   -0.139472    1.188050
    3          1             0       -0.795301    1.244514    0.068950
    4          1             0       -0.176611    0.380299    2.204987
    5          8             0       -0.060071   -0.318664   -1.090793
    6          1             0       -0.410721   -1.112624    0.803496
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.033686    0.483630   -0.183171
    2          7             0        0.198510   -0.123887    1.194336
    3          1             0       -0.631522    1.249859    0.117040
    4          1             0       -0.156607    0.505567    2.051047
    5          8             0       -0.076151   -0.298540   -1.098876
    6          1             0       -0.360447   -1.057487    0.859929
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.050390    0.408900   -0.160360
    2          7             0        0.216125   -0.106917    1.191658
    3          1             0       -0.457784    1.474656    0.140353
    4          1             0       -0.151420    0.607182    1.890756
    5          8             0       -0.094181   -0.278118   -1.112419
    6          1             0       -0.299058   -1.054037    0.977445
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.069940    0.349887   -0.106142
    2          7             0        0.241238   -0.090604    1.169433
    3          1             0       -0.279660    1.709005    0.062415
    4          1             0       -0.198884    0.692503    1.785152
    5          8             0       -0.112533   -0.263347   -1.134164
    6          1             0       -0.254593   -1.132131    1.169313
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.090381    0.337164   -0.023741
    2          7             0        0.267307   -0.078043    1.130932
    3          1             0       -0.082630    1.644565   -0.097471
    4          1             0       -0.257797    0.730282    1.708729
    5          8             0       -0.130751   -0.260773   -1.164001
    6          1             0       -0.222381   -1.169352    1.432966
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.104779    0.355580    0.054792
    2          7             0        0.289389   -0.073890    1.089833
    3          1             0        0.091439    1.367875   -0.269995
    4          1             0       -0.301995    0.752128    1.692125
    5          8             0       -0.148359   -0.265601   -1.190507
    6          1             0       -0.208144   -1.114858    1.678720
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.112849    0.373749    0.101421
    2          7             0        0.306606   -0.076933    1.059588
    3          1             0        0.224195    1.223641   -0.415743
    4          1             0       -0.314192    0.811803    1.776753
    5          8             0       -0.167768   -0.272096   -1.202729
    6          1             0       -0.163800   -1.101280    1.798874
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.114828    0.370472    0.110034
    2          7             0        0.318959   -0.079374    1.045654
    3          1             0        0.313033    1.421367   -0.500814
    4          1             0       -0.264096    0.850773    1.922885
    5          8             0       -0.189134   -0.276414   -1.200181
    6          1             0       -0.111682   -1.196516    1.788411
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.105014    0.373346    0.066914
    2          7             0        0.322043   -0.085094    1.054429
    3          1             0        0.361106    1.633444   -0.434538
    4          1             0       -0.151728    0.849414    2.066565
    5          8             0       -0.211907   -0.282556   -1.181569
    6          1             0       -0.070402   -1.264498    1.674598
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.084641    0.405660   -0.015866
    2          7             0        0.314125   -0.089155    1.094033
    3          1             0        0.377344    1.549512   -0.218711
    4          1             0        0.064771    0.754664    2.070415
    5          8             0       -0.235028   -0.295297   -1.154928
    6          1             0       -0.068721   -1.211950    1.467480
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.053256    0.454814   -0.101193
    2          7             0        0.289785   -0.077299    1.162288
    3          1             0        0.389559    1.328465    0.070362
    4          1             0        0.475366    0.490022    1.794723
    5          8             0       -0.263389   -0.313761   -1.136115
    6          1             0       -0.076890   -1.183222    1.223106
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.017537    0.496333   -0.184403
    2          7             0        0.265662   -0.081498    1.218179
    3          1             0        0.417440    1.319308    0.335054
    4          1             0        0.962521    0.297037    1.624219
    5          8             0       -0.298594   -0.328730   -1.114904
    6          1             0       -0.123287   -1.179533    1.006463
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.003968    0.506160   -0.248632
    2          7             0        0.243462   -0.101187    1.263866
    3          1             0        0.523344    1.613744    0.493043
    4          1             0        1.489394    0.153307    1.529609
    5          8             0       -0.333381   -0.337333   -1.096318
    6          1             0       -0.151516   -1.037170    0.778082
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.015661    0.504926   -0.272292
    2          7             0        0.232534   -0.107746    1.301732
    3          1             0        0.633827    1.871167    0.530374
    4          1             0        1.748395   -0.016588    1.366545
    5          8             0       -0.356724   -0.343609   -1.091971
    6          1             0       -0.137902   -0.919276    0.590384
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.015661    0.504926   -0.272292
    2          7             0        0.232534   -0.107746    1.301732
    3          1             0        0.633827    1.871167    0.530374
    4          1             0        1.748395   -0.016588    1.366545
    5          8             0       -0.356724   -0.343609   -1.091971
    6          1             0       -0.137902   -0.919276    0.590384
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.015661    0.504926   -0.272292
    2          7             0        0.232534   -0.107746    1.301732
    3          1             0        0.633827    1.871167    0.530374
    4          1             0        1.748395   -0.016588    1.366545
    5          8             0       -0.356724   -0.343609   -1.091971
    6          1             0       -0.137902   -0.919276    0.590384
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.015661    0.504926   -0.272292
    2          7             0        0.232534   -0.107746    1.301732
    3          1             0        0.633827    1.871167    0.530374
    4          1             0        1.748395   -0.016588    1.366545
    5          8             0       -0.356724   -0.343609   -1.091971
    6          1             0       -0.137902   -0.919276    0.590384
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.015661    0.504926   -0.272292
    2          7             0        0.232534   -0.107746    1.301732
    3          1             0        0.633827    1.871167    0.530374
    4          1             0        1.748395   -0.016588    1.366545
    5          8             0       -0.356724   -0.343609   -1.091971
    6          1             0       -0.137902   -0.919276    0.590384
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.015661    0.504926   -0.272292
    2          7             0        0.232534   -0.107746    1.301732
    3          1             0        0.633827    1.871167    0.530374
    4          1             0        1.748395   -0.016588    1.366545
    5          8             0       -0.356724   -0.343609   -1.091971
    6          1             0       -0.137902   -0.919276    0.590384
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.015661    0.504926   -0.272292
    2          7             0        0.232534   -0.107746    1.301732
    3          1             0        0.633827    1.871167    0.530374
    4          1             0        1.748395   -0.016588    1.366545
    5          8             0       -0.356724   -0.343609   -1.091971
    6          1             0       -0.137902   -0.919276    0.590384
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.015661    0.504926   -0.272292
    2          7             0        0.232534   -0.107746    1.301732
    3          1             0        0.633827    1.871167    0.530374
    4          1             0        1.748395   -0.016588    1.366545
    5          8             0       -0.356724   -0.343609   -1.091971
    6          1             0       -0.137902   -0.919276    0.590384
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.015661    0.504926   -0.272292
    2          7             0        0.232534   -0.107746    1.301732
    3          1             0        0.633827    1.871167    0.530374
    4          1             0        1.748395   -0.016588    1.366545
    5          8             0       -0.356724   -0.343609   -1.091971
    6          1             0       -0.137902   -0.919276    0.590384
 ---------------------------------------------------------------------
