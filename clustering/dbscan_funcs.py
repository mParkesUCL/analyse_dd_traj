"""
Function to perform DBSCAN clustering algorithm.
"""

import math
import numpy as np
from sklearn.decomposition import PCA
from sklearn.cluster import DBSCAN
import IO.io_funcs as iof


def get_trajs(filename, ntraj):
    # Get trajectory geoms in "trajs"
    trajs = []
    filelist = []
    num_traj_list = []
    for i in range(ntraj):
        # generate file name to load
        cur_file = "ddtraj_" + str(i + 1) + ".pl"
        cur_file = iof.path.join(filename, cur_file)
        # load the file
        n_geom, geom = iof.load_xyz(cur_file)
        # These will make sense as a trajectory class with properties
        trajs.append(geom)
        filelist.append(cur_file)
        num_traj_list.append(n_geom)

    return trajs, n_geom


def calc_mean_traj(traj_dir, n_traj, cluster, idx):
    trajs, n_geom = get_trajs(traj_dir, n_traj)
    open(traj_dir + "cluster_{}_mean.xyz".format(idx), "w")

    for j in range(n_geom):
        zeros_coord = [
            [0 for _ in range(3)] for _ in range(len(trajs[0][0].get_atom()))
        ]
        av_coord = zeros_coord
        atm_names = trajs[1][j].get_atom()
        for i in cluster:
            coord_2 = trajs[i - 1][j].get_coords()
            for atoms_1, atoms_2 in zip(av_coord, coord_2):
                # get positions of the first atom
                av_coord[av_coord.index(atoms_1)] = [
                    atoms_1[0] + atoms_2[0],
                    atoms_1[1] + atoms_2[1],
                    atoms_1[2] + atoms_2[2],
                ]
        for atoms in av_coord:
            av_coord[av_coord.index(atoms)] = [
                atoms[0] / (len(cluster)),
                atoms[1] / (len(cluster)),
                atoms[2] / (len(cluster)),
            ]

        with open(traj_dir + "cluster_{}_mean.xyz".format(idx), "a") as file:
            file.write("{}\n\n".format(str(len(av_coord))))
            for k in range(len(av_coord)):
                file.write(str(atm_names[k]) + "      ")
                for num in av_coord[k]:
                    file.write(str(num) + "   ")
                file.write("\n")
            file.write("\n\n\n")


def dbscan_cluster(F, e, dir, ntrajs):
    # Perform DBSCAN clustering
    dbscan = DBSCAN(eps=e, min_samples=2, metric="precomputed").fit(F)
    labels = dbscan.labels_

    # Number of clusters in labels, ignoring noise if present.
    n_clusters = len(set(labels)) - (1 if -1 in labels else 0)
    n_noise = list(labels).count(-1)

    print(" * EPS value: %.2f" % e)
    print(" * Number of clusters: %d" % n_clusters)
    print(" * Number of noise points: %d\n" % n_noise)

    # Obtain labels for noisy trajs
    X = dbscan.core_sample_indices_
    Y = np.array(range(len(F[0])))
    noise = np.setdiff1d(Y, X)

    # Print out clustering outcome
    C = {}
    C["C0"] = list(noise + 1)
    for i in range(n_clusters):
        temp = []
        for j in range(len(labels)):
            if labels[j] == i:
                temp.append(j + 1)
                C["C" + str(i + 1)] = temp
        print("Cluster {} trajs: {}".format(i + 1, C["C" + str(i + 1)]))
    print("Noisy trajs:     {}\n".format(C["C0"]))

    # Sensitivity Analysis:
    print("Sensitivity Analysis: \n")
    print("  EPS  |  # Clusters |  # noise")
    print(" ------------------------------")
    EPS = 0.1
    for i in range(1, 11):
        dbscan_s = DBSCAN(eps=EPS, min_samples=2, metric="precomputed").fit(F)
        labs_s = dbscan_s.labels_
        n_clu_s = len(set(labs_s)) - (1 if -1 in labs_s else 0)
        n_no_s = list(labs_s).count(-1)
        print(" %.2f  |      {}      |     {}   ".format(n_clu_s, n_no_s) % EPS)
        EPS = EPS + 0.1

    # Write mean trajectory of each cluster:
    print("\nWriting mean trajectory for each cluster in cluster_n_mean.xyz ...")

    for idx in range(1, len(C)):
        cluster = C["C" + str(idx)]
        calc_mean_traj(dir, ntrajs, cluster, idx)

    return
