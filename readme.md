# DD Analyse

Simple programme to load a trajectory file in xyz format to perform simple statistical analysis

## Usage

Programme is driven from the command line. Use the -h option to see what the command line options are. An example trajectory file is provided in the examples folder.

Running for a single named file:
```bash
python traj_main.py -file ./example/ddtraj_1.pl -rmsd
```
Will perform a RMSD calculation on the trajectory file and plot the result to screen

Running for all the files:
```bash
python traj_main.py -dir ./examples -nfiles 10 -pca --npca 2
```
Will load the 10 ddtraj_ files found in the examples folder and perform a pca with 2 components

To run without showing plots add the -np flag

An example Jupyter notebook is provided to demonstrate how to use the library
```bash
traj_note.ipynb
```

## Installation

To perform PCA analysis requires SciKit learn to be installed. This currently seems to have some incompatible with python 3.11 but works with 3.10.

A conda environment file (sklearn-env.yml) is provided that will install the necessary packages with working versions
```
conda env create -f sklearn-env.yml
```

## Testing and formatting (Work in progress)

Testing is done through the pytest framework

Formatting is done using black