"""
Functions to calaculate Fréchet dist. between trajs
"""
import sys
import numpy as np
from sklearn.cluster import DBSCAN
from openeye import oechem
import matplotlib.pyplot as plt


def RMSD(i, j, geo, T1, T2):
    """Calculates RMSD between two structures

    Args:
        i (int): index of structure 1
        j (int): index of structure 2
        geo (list): The geometries from all trajectories
        T1 (int): Index of trajectory 1
        T2 (int): Index of trajectory 2

    Returns:
        float: RMSD between the two structures
    """
    x1 = oechem.OEFloatArray(geo[T1 - 1][i])
    x2 = oechem.OEFloatArray(geo[T2 - 1][j])
    rmsd = oechem.OERMSD(x1, x2, int(len(geo[0][0]) / 3), True)
    return rmsd


def dist_mat(size, geo, T1, T2):
    """Function to get distance matrix for each frame of trajectory

    Args:
        size (int): Number of frames in trajectory
        geo (list): The geometries from all trajectories
        T1 (int): the index of the first trajectory
        T2 (int): the index of the second trajectory

    Returns:
        D : the distance matrix
    """
    D = [[0] * size for _ in range(size)]
    for i in range(size):
        for j in range(size):
            D[i][j] = RMSD(i, j, geo, T1, T2)
    return D


def Frechet(geo, ntraj, frames, T1, T2):
    """Calculate the Frechet distance between two trajectories

    Args:
        geo (list): The geometries from all trajectories
        ntraj (int): Number of trajectories
        frames (int): Number of frames in trajectory
        T1 (int): the index of the first trajectory
        T2 (int): the index of the second trajectory

    Returns:
       dF: List of Frechet values
    """
    D_rmsd = dist_mat(frames, geo, T1, T2)

    f = np.ones((frames, frames)) * -1

    f[0, 0] = D_rmsd[0][0]
    # load the first column and first row with distances (memorize)
    for i in range(1, frames):
        f[i, 0] = max(f[i - 1, 0], D_rmsd[i][0])
    for j in range(1, frames):
        f[0, j] = max(f[0, j - 1], D_rmsd[0][j])

    for i in range(1, frames):
        for j in range(1, frames):
            f[i, j] = max(min(f[i - 1, j], f[i, j - 1], f[i - 1, j - 1]), D_rmsd[i][j])
            dF = f[frames - 1, frames - 1]

    return dF
