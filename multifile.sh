#!/bin/bash -l
#Run the traj analysis on more than one file


if [ "$1" = "-h" ]; then
  echo "   "
  echo "Script to run PCA analysis on many files"
  echo "To run, be in directory with files to alter"
  echo "multifile.sh <dir with ddtraj files> <number of trajectories>"
  echo "   "
  exit 0
fi

DIR=$1
NUM=$2

for ((j=1; j<=$NUM; j++))   #loop over trajectories
do
    CLI="python pca_main.py -file ${DIR}/ddtraj_${j}.pl -rmsd -np -pca -ofile ${DIR}/out_${j}_ -save"
    echo $CLI
    $CLI
done


