
 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.000000    0.401721   -0.096031
    2          7             0        0.000000   -0.162504    1.145371
    3          1             0        0.000000    1.541283   -0.009698
    4          1             0        0.000000    0.374796    1.979555
    5          8             0        0.000000   -0.207339   -1.131411
    6          1             0        0.000000   -1.153754    1.217289
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.005873    0.426928   -0.082885
    2          7             0        0.009743   -0.187497    1.136894
    3          1             0       -0.003367    1.512856   -0.054221
    4          1             0       -0.034691    0.590413    2.007165
    5          8             0       -0.005692   -0.215367   -1.134669
    6          1             0        0.062960   -1.166401    1.247163
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.009762    0.451208   -0.074685
    2          7             0        0.018624   -0.236147    1.078242
    3          1             0       -0.030676    1.431051   -0.216265
    4          1             0       -0.077867    1.071260    2.574051
    5          8             0       -0.005434   -0.210343   -1.119609
    6          1             0        0.052272   -1.258345    1.320671
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.019078    0.494966   -0.065524
    2          7             0        0.036094   -0.291063    1.024899
    3          1             0       -0.051690    1.459006   -0.363153
    4          1             0       -0.141169    1.493158    3.077699
    5          8             0       -0.007860   -0.217497   -1.106887
    6          1             0        0.043298   -1.352676    1.394143
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.032561    0.545256   -0.065395
    2          7             0        0.060458   -0.346145    0.979190
    3          1             0       -0.077629    1.567701   -0.486265
    4          1             0       -0.217876    1.867651    3.557358
    5          8             0       -0.012257   -0.231934   -1.091854
    6          1             0        0.037750   -1.440225    1.432611
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.048647    0.595056   -0.082646
    2          7             0        0.085905   -0.400296    0.943707
    3          1             0       -0.090100    1.658005   -0.567553
    4          1             0       -0.287982    2.207192    4.028097
    5          8             0       -0.018557   -0.249468   -1.071363
    6          1             0        0.058292   -1.432379    1.416420
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.066406    0.638514   -0.118046
    2          7             0        0.113679   -0.448838    0.917905
    3          1             0       -0.102916    1.658283   -0.601428
    4          1             0       -0.351710    2.523670    4.492296
    5          8             0       -0.027267   -0.267743   -1.045211
    6          1             0        0.098611   -1.302114    1.351103
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.073984    0.653229   -0.136294
    2          7             0        0.126095   -0.462778    0.909123
    3          1             0       -0.109942    1.638956   -0.597894
    4          1             0       -0.384842    2.642410    4.673496
    5          8             0       -0.031805   -0.275149   -1.033731
    6          1             0        0.128521   -1.265513    1.323476
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.073984    0.653229   -0.136294
    2          7             0        0.126095   -0.462778    0.909123
    3          1             0       -0.109942    1.638956   -0.597894
    4          1             0       -0.384842    2.642410    4.673496
    5          8             0       -0.031805   -0.275149   -1.033731
    6          1             0        0.128521   -1.265513    1.323476
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.073984    0.653229   -0.136294
    2          7             0        0.126095   -0.462778    0.909123
    3          1             0       -0.109942    1.638956   -0.597894
    4          1             0       -0.384842    2.642410    4.673496
    5          8             0       -0.031805   -0.275149   -1.033731
    6          1             0        0.128521   -1.265513    1.323476
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.073984    0.653229   -0.136294
    2          7             0        0.126095   -0.462778    0.909123
    3          1             0       -0.109942    1.638956   -0.597894
    4          1             0       -0.384842    2.642410    4.673496
    5          8             0       -0.031805   -0.275149   -1.033731
    6          1             0        0.128521   -1.265513    1.323476
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.073984    0.653229   -0.136294
    2          7             0        0.126095   -0.462778    0.909123
    3          1             0       -0.109942    1.638956   -0.597894
    4          1             0       -0.384842    2.642410    4.673496
    5          8             0       -0.031805   -0.275149   -1.033731
    6          1             0        0.128521   -1.265513    1.323476
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.073984    0.653229   -0.136294
    2          7             0        0.126095   -0.462778    0.909123
    3          1             0       -0.109942    1.638956   -0.597894
    4          1             0       -0.384842    2.642410    4.673496
    5          8             0       -0.031805   -0.275149   -1.033731
    6          1             0        0.128521   -1.265513    1.323476
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.073984    0.653229   -0.136294
    2          7             0        0.126095   -0.462778    0.909123
    3          1             0       -0.109942    1.638956   -0.597894
    4          1             0       -0.384842    2.642410    4.673496
    5          8             0       -0.031805   -0.275149   -1.033731
    6          1             0        0.128521   -1.265513    1.323476
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.073984    0.653229   -0.136294
    2          7             0        0.126095   -0.462778    0.909123
    3          1             0       -0.109942    1.638956   -0.597894
    4          1             0       -0.384842    2.642410    4.673496
    5          8             0       -0.031805   -0.275149   -1.033731
    6          1             0        0.128521   -1.265513    1.323476
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.073984    0.653229   -0.136294
    2          7             0        0.126095   -0.462778    0.909123
    3          1             0       -0.109942    1.638956   -0.597894
    4          1             0       -0.384842    2.642410    4.673496
    5          8             0       -0.031805   -0.275149   -1.033731
    6          1             0        0.128521   -1.265513    1.323476
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.073984    0.653229   -0.136294
    2          7             0        0.126095   -0.462778    0.909123
    3          1             0       -0.109942    1.638956   -0.597894
    4          1             0       -0.384842    2.642410    4.673496
    5          8             0       -0.031805   -0.275149   -1.033731
    6          1             0        0.128521   -1.265513    1.323476
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.073984    0.653229   -0.136294
    2          7             0        0.126095   -0.462778    0.909123
    3          1             0       -0.109942    1.638956   -0.597894
    4          1             0       -0.384842    2.642410    4.673496
    5          8             0       -0.031805   -0.275149   -1.033731
    6          1             0        0.128521   -1.265513    1.323476
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.073984    0.653229   -0.136294
    2          7             0        0.126095   -0.462778    0.909123
    3          1             0       -0.109942    1.638956   -0.597894
    4          1             0       -0.384842    2.642410    4.673496
    5          8             0       -0.031805   -0.275149   -1.033731
    6          1             0        0.128521   -1.265513    1.323476
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.073984    0.653229   -0.136294
    2          7             0        0.126095   -0.462778    0.909123
    3          1             0       -0.109942    1.638956   -0.597894
    4          1             0       -0.384842    2.642410    4.673496
    5          8             0       -0.031805   -0.275149   -1.033731
    6          1             0        0.128521   -1.265513    1.323476
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.073984    0.653229   -0.136294
    2          7             0        0.126095   -0.462778    0.909123
    3          1             0       -0.109942    1.638956   -0.597894
    4          1             0       -0.384842    2.642410    4.673496
    5          8             0       -0.031805   -0.275149   -1.033731
    6          1             0        0.128521   -1.265513    1.323476
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.073984    0.653229   -0.136294
    2          7             0        0.126095   -0.462778    0.909123
    3          1             0       -0.109942    1.638956   -0.597894
    4          1             0       -0.384842    2.642410    4.673496
    5          8             0       -0.031805   -0.275149   -1.033731
    6          1             0        0.128521   -1.265513    1.323476
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.073984    0.653229   -0.136294
    2          7             0        0.126095   -0.462778    0.909123
    3          1             0       -0.109942    1.638956   -0.597894
    4          1             0       -0.384842    2.642410    4.673496
    5          8             0       -0.031805   -0.275149   -1.033731
    6          1             0        0.128521   -1.265513    1.323476
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.073984    0.653229   -0.136294
    2          7             0        0.126095   -0.462778    0.909123
    3          1             0       -0.109942    1.638956   -0.597894
    4          1             0       -0.384842    2.642410    4.673496
    5          8             0       -0.031805   -0.275149   -1.033731
    6          1             0        0.128521   -1.265513    1.323476
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.073984    0.653229   -0.136294
    2          7             0        0.126095   -0.462778    0.909123
    3          1             0       -0.109942    1.638956   -0.597894
    4          1             0       -0.384842    2.642410    4.673496
    5          8             0       -0.031805   -0.275149   -1.033731
    6          1             0        0.128521   -1.265513    1.323476
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.073984    0.653229   -0.136294
    2          7             0        0.126095   -0.462778    0.909123
    3          1             0       -0.109942    1.638956   -0.597894
    4          1             0       -0.384842    2.642410    4.673496
    5          8             0       -0.031805   -0.275149   -1.033731
    6          1             0        0.128521   -1.265513    1.323476
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.073984    0.653229   -0.136294
    2          7             0        0.126095   -0.462778    0.909123
    3          1             0       -0.109942    1.638956   -0.597894
    4          1             0       -0.384842    2.642410    4.673496
    5          8             0       -0.031805   -0.275149   -1.033731
    6          1             0        0.128521   -1.265513    1.323476
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.073984    0.653229   -0.136294
    2          7             0        0.126095   -0.462778    0.909123
    3          1             0       -0.109942    1.638956   -0.597894
    4          1             0       -0.384842    2.642410    4.673496
    5          8             0       -0.031805   -0.275149   -1.033731
    6          1             0        0.128521   -1.265513    1.323476
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.073984    0.653229   -0.136294
    2          7             0        0.126095   -0.462778    0.909123
    3          1             0       -0.109942    1.638956   -0.597894
    4          1             0       -0.384842    2.642410    4.673496
    5          8             0       -0.031805   -0.275149   -1.033731
    6          1             0        0.128521   -1.265513    1.323476
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.073984    0.653229   -0.136294
    2          7             0        0.126095   -0.462778    0.909123
    3          1             0       -0.109942    1.638956   -0.597894
    4          1             0       -0.384842    2.642410    4.673496
    5          8             0       -0.031805   -0.275149   -1.033731
    6          1             0        0.128521   -1.265513    1.323476
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.073984    0.653229   -0.136294
    2          7             0        0.126095   -0.462778    0.909123
    3          1             0       -0.109942    1.638956   -0.597894
    4          1             0       -0.384842    2.642410    4.673496
    5          8             0       -0.031805   -0.275149   -1.033731
    6          1             0        0.128521   -1.265513    1.323476
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.073984    0.653229   -0.136294
    2          7             0        0.126095   -0.462778    0.909123
    3          1             0       -0.109942    1.638956   -0.597894
    4          1             0       -0.384842    2.642410    4.673496
    5          8             0       -0.031805   -0.275149   -1.033731
    6          1             0        0.128521   -1.265513    1.323476
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.073984    0.653229   -0.136294
    2          7             0        0.126095   -0.462778    0.909123
    3          1             0       -0.109942    1.638956   -0.597894
    4          1             0       -0.384842    2.642410    4.673496
    5          8             0       -0.031805   -0.275149   -1.033731
    6          1             0        0.128521   -1.265513    1.323476
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.073984    0.653229   -0.136294
    2          7             0        0.126095   -0.462778    0.909123
    3          1             0       -0.109942    1.638956   -0.597894
    4          1             0       -0.384842    2.642410    4.673496
    5          8             0       -0.031805   -0.275149   -1.033731
    6          1             0        0.128521   -1.265513    1.323476
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.073984    0.653229   -0.136294
    2          7             0        0.126095   -0.462778    0.909123
    3          1             0       -0.109942    1.638956   -0.597894
    4          1             0       -0.384842    2.642410    4.673496
    5          8             0       -0.031805   -0.275149   -1.033731
    6          1             0        0.128521   -1.265513    1.323476
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.073984    0.653229   -0.136294
    2          7             0        0.126095   -0.462778    0.909123
    3          1             0       -0.109942    1.638956   -0.597894
    4          1             0       -0.384842    2.642410    4.673496
    5          8             0       -0.031805   -0.275149   -1.033731
    6          1             0        0.128521   -1.265513    1.323476
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.073984    0.653229   -0.136294
    2          7             0        0.126095   -0.462778    0.909123
    3          1             0       -0.109942    1.638956   -0.597894
    4          1             0       -0.384842    2.642410    4.673496
    5          8             0       -0.031805   -0.275149   -1.033731
    6          1             0        0.128521   -1.265513    1.323476
 ---------------------------------------------------------------------
