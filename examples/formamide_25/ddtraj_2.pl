
 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.000000    0.401721   -0.096031
    2          7             0        0.000000   -0.162504    1.145371
    3          1             0        0.000000    1.541283   -0.009698
    4          1             0        0.000000    0.374796    1.979555
    5          8             0        0.000000   -0.207339   -1.131411
    6          1             0        0.000000   -1.153754    1.217289
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.003032    0.417027   -0.093535
    2          7             0       -0.002061   -0.169238    1.132062
    3          1             0       -0.017708    1.488266   -0.069409
    4          1             0        0.035396    0.425565    2.131464
    5          8             0        0.002997   -0.210688   -1.127029
    6          1             0       -0.000514   -1.187029    1.210766
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.010461    0.459231   -0.066953
    2          7             0       -0.002135   -0.175753    1.155486
    3          1             0       -0.041389    1.462454   -0.138559
    4          1             0        0.048049    0.355683    1.866798
    5          8             0        0.005094   -0.231436   -1.141945
    6          1             0        0.066730   -1.174046    1.139320
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.008389    0.499592   -0.015847
    2          7             0        0.016960   -0.201289    1.109899
    3          1             0       -0.053666    1.486511   -0.289691
    4          1             0       -0.062512    0.419710    2.039814
    5          8             0       -0.002012   -0.244997   -1.153650
    6          1             0        0.012336   -1.172668    1.328109
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.000240    0.524663    0.011319
    2          7             0        0.020967   -0.206554    1.112893
    3          1             0       -0.152047    1.602683   -0.406824
    4          1             0       -0.046770    0.319983    1.977591
    5          8             0       -0.003564   -0.259356   -1.162544
    6          1             0       -0.038805   -1.186585    1.283551
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.000757    0.584452    0.024532
    2          7             0        0.028114   -0.242717    1.116279
    3          1             0       -0.090874    1.742694   -0.456288
    4          1             0        0.012984    0.273530    1.924271
    5          8             0       -0.011051   -0.278939   -1.167640
    6          1             0       -0.128336   -1.178770    1.262844
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.001904    0.634705    0.011943
    2          7             0        0.034505   -0.264979    1.117211
    3          1             0       -0.025312    1.763801   -0.515924
    4          1             0        0.030885    0.305607    2.072777
    5          8             0       -0.021387   -0.294492   -1.157383
    6          1             0       -0.122882   -1.274168    1.148148
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.004568    0.673043   -0.012739
    2          7             0        0.035263   -0.262420    1.131423
    3          1             0       -0.033196    1.678132   -0.419631
    4          1             0        0.024282    0.232642    2.018126
    5          8             0       -0.023151   -0.311505   -1.151697
    6          1             0       -0.059215   -1.337556    1.112669
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.020126    0.681560   -0.056048
    2          7             0        0.035052   -0.254431    1.152645
    3          1             0        0.021125    1.638482   -0.317882
    4          1             0        0.017208    0.156805    2.029877
    5          8             0       -0.024164   -0.324087   -1.137205
    6          1             0        0.097804   -1.234784    0.989998
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.029942    0.670855   -0.094084
    2          7             0        0.037303   -0.245898    1.162149
    3          1             0        0.066177    1.741162   -0.244401
    4          1             0        0.028279    0.075391    2.125761
    5          8             0       -0.026787   -0.328468   -1.121961
    6          1             0        0.168899   -1.177609    0.899536
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.046176    0.651674   -0.129769
    2          7             0        0.041985   -0.226103    1.167008
    3          1             0        0.126858    1.837872   -0.170539
    4          1             0        0.043378   -0.021715    2.239367
    5          8             0       -0.029953   -0.332494   -1.107228
    6          1             0        0.271617   -1.159961    0.835640
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.061870    0.642472   -0.153257
    2          7             0        0.046582   -0.202515    1.175206
    3          1             0        0.190438    1.782584   -0.078207
    4          1             0        0.050772   -0.140524    2.225321
    5          8             0       -0.030129   -0.335621   -1.100921
    6          1             0        0.326436   -1.154406    0.823008
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.080891    0.644030   -0.173993
    2          7             0        0.052008   -0.187238    1.182640
    3          1             0        0.286554    1.621584    0.027031
    4          1             0        0.039682   -0.239305    2.183043
    5          8             0       -0.027128   -0.338046   -1.096750
    6          1             0        0.344875   -1.086946    0.837449
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.109955    0.641951   -0.204956
    2          7             0        0.059569   -0.183162    1.187379
    3          1             0        0.464329    1.540532    0.137744
    4          1             0       -0.006034   -0.296571    2.221923
    5          8             0       -0.022109   -0.337692   -1.086737
    6          1             0        0.374184   -0.986135    0.831774
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.151120    0.631179   -0.247612
    2          7             0        0.066904   -0.185667    1.200831
    3          1             0        0.764793    1.615997    0.261745
    4          1             0       -0.075396   -0.322224    2.259231
    5          8             0       -0.016505   -0.332736   -1.072521
    6          1             0        0.442392   -0.951536    0.765829
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.194619    0.630836   -0.293784
    2          7             0        0.070788   -0.189289    1.225936
    3          1             0        1.067630    1.641928    0.390203
    4          1             0       -0.088054   -0.353172    2.229741
    5          8             0       -0.007506   -0.328340   -1.058655
    6          1             0        0.473382   -0.961862    0.647750
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.233041    0.650935   -0.359003
    2          7             0        0.066447   -0.187314    1.253596
    3          1             0        1.148968    1.570965    0.518998
    4          1             0        0.033458   -0.445732    2.308017
    5          8             0        0.013769   -0.335989   -1.034675
    6          1             0        0.450699   -0.943693    0.452322
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.272187    0.684651   -0.452945
    2          7             0        0.049788   -0.180859    1.269640
    3          1             0        1.002326    1.601436    0.706693
    4          1             0        0.246078   -0.688934    2.591503
    5          8             0        0.056630   -0.358488   -0.996074
    6          1             0        0.402046   -0.865018    0.264187
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.329924    0.736747   -0.605825
    2          7             0        0.016855   -0.162988    1.331519
    3          1             0        0.870901    1.609953    1.079986
    4          1             0        0.711965   -1.185628    2.710971
    5          8             0        0.114773   -0.391235   -0.943441
    6          1             0        0.289851   -0.725693   -0.103360
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.363410    0.766758   -0.700495
    2          7             0       -0.003007   -0.151447    1.380139
    3          1             0        0.877183    1.609587    1.312942
    4          1             0        0.979548   -1.501731    2.707768
    5          8             0        0.143864   -0.407733   -0.912762
    6          1             0        0.228976   -0.665082   -0.368338
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.363410    0.766758   -0.700495
    2          7             0       -0.003007   -0.151447    1.380139
    3          1             0        0.877183    1.609587    1.312942
    4          1             0        0.979548   -1.501731    2.707768
    5          8             0        0.143864   -0.407733   -0.912762
    6          1             0        0.228976   -0.665082   -0.368338
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.363410    0.766758   -0.700495
    2          7             0       -0.003007   -0.151447    1.380139
    3          1             0        0.877183    1.609587    1.312942
    4          1             0        0.979548   -1.501731    2.707768
    5          8             0        0.143864   -0.407733   -0.912762
    6          1             0        0.228976   -0.665082   -0.368338
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.363410    0.766758   -0.700495
    2          7             0       -0.003007   -0.151447    1.380139
    3          1             0        0.877183    1.609587    1.312942
    4          1             0        0.979548   -1.501731    2.707768
    5          8             0        0.143864   -0.407733   -0.912762
    6          1             0        0.228976   -0.665082   -0.368338
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.363410    0.766758   -0.700495
    2          7             0       -0.003007   -0.151447    1.380139
    3          1             0        0.877183    1.609587    1.312942
    4          1             0        0.979548   -1.501731    2.707768
    5          8             0        0.143864   -0.407733   -0.912762
    6          1             0        0.228976   -0.665082   -0.368338
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.363410    0.766758   -0.700495
    2          7             0       -0.003007   -0.151447    1.380139
    3          1             0        0.877183    1.609587    1.312942
    4          1             0        0.979548   -1.501731    2.707768
    5          8             0        0.143864   -0.407733   -0.912762
    6          1             0        0.228976   -0.665082   -0.368338
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.363410    0.766758   -0.700495
    2          7             0       -0.003007   -0.151447    1.380139
    3          1             0        0.877183    1.609587    1.312942
    4          1             0        0.979548   -1.501731    2.707768
    5          8             0        0.143864   -0.407733   -0.912762
    6          1             0        0.228976   -0.665082   -0.368338
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.363410    0.766758   -0.700495
    2          7             0       -0.003007   -0.151447    1.380139
    3          1             0        0.877183    1.609587    1.312942
    4          1             0        0.979548   -1.501731    2.707768
    5          8             0        0.143864   -0.407733   -0.912762
    6          1             0        0.228976   -0.665082   -0.368338
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.363410    0.766758   -0.700495
    2          7             0       -0.003007   -0.151447    1.380139
    3          1             0        0.877183    1.609587    1.312942
    4          1             0        0.979548   -1.501731    2.707768
    5          8             0        0.143864   -0.407733   -0.912762
    6          1             0        0.228976   -0.665082   -0.368338
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.363410    0.766758   -0.700495
    2          7             0       -0.003007   -0.151447    1.380139
    3          1             0        0.877183    1.609587    1.312942
    4          1             0        0.979548   -1.501731    2.707768
    5          8             0        0.143864   -0.407733   -0.912762
    6          1             0        0.228976   -0.665082   -0.368338
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.363410    0.766758   -0.700495
    2          7             0       -0.003007   -0.151447    1.380139
    3          1             0        0.877183    1.609587    1.312942
    4          1             0        0.979548   -1.501731    2.707768
    5          8             0        0.143864   -0.407733   -0.912762
    6          1             0        0.228976   -0.665082   -0.368338
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.363410    0.766758   -0.700495
    2          7             0       -0.003007   -0.151447    1.380139
    3          1             0        0.877183    1.609587    1.312942
    4          1             0        0.979548   -1.501731    2.707768
    5          8             0        0.143864   -0.407733   -0.912762
    6          1             0        0.228976   -0.665082   -0.368338
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.363410    0.766758   -0.700495
    2          7             0       -0.003007   -0.151447    1.380139
    3          1             0        0.877183    1.609587    1.312942
    4          1             0        0.979548   -1.501731    2.707768
    5          8             0        0.143864   -0.407733   -0.912762
    6          1             0        0.228976   -0.665082   -0.368338
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.363410    0.766758   -0.700495
    2          7             0       -0.003007   -0.151447    1.380139
    3          1             0        0.877183    1.609587    1.312942
    4          1             0        0.979548   -1.501731    2.707768
    5          8             0        0.143864   -0.407733   -0.912762
    6          1             0        0.228976   -0.665082   -0.368338
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.363410    0.766758   -0.700495
    2          7             0       -0.003007   -0.151447    1.380139
    3          1             0        0.877183    1.609587    1.312942
    4          1             0        0.979548   -1.501731    2.707768
    5          8             0        0.143864   -0.407733   -0.912762
    6          1             0        0.228976   -0.665082   -0.368338
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.363410    0.766758   -0.700495
    2          7             0       -0.003007   -0.151447    1.380139
    3          1             0        0.877183    1.609587    1.312942
    4          1             0        0.979548   -1.501731    2.707768
    5          8             0        0.143864   -0.407733   -0.912762
    6          1             0        0.228976   -0.665082   -0.368338
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.363410    0.766758   -0.700495
    2          7             0       -0.003007   -0.151447    1.380139
    3          1             0        0.877183    1.609587    1.312942
    4          1             0        0.979548   -1.501731    2.707768
    5          8             0        0.143864   -0.407733   -0.912762
    6          1             0        0.228976   -0.665082   -0.368338
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.363410    0.766758   -0.700495
    2          7             0       -0.003007   -0.151447    1.380139
    3          1             0        0.877183    1.609587    1.312942
    4          1             0        0.979548   -1.501731    2.707768
    5          8             0        0.143864   -0.407733   -0.912762
    6          1             0        0.228976   -0.665082   -0.368338
 ---------------------------------------------------------------------
