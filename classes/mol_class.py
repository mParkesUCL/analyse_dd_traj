"""
A class for molecules
"""


class Mol:
    """Class to contain the molecular coordinates"""

    def __init__(self, cent_nums, atom_nums, coords) -> None:
        """_summary_

        Args:
            cent_nums (_int_): list of the centre numbers
            atom_nums (_int_): list of the atom numbers
            coords (_float_): list of the atomic coordinates, a list of lists. \
                                where each row is [x,y,z]
        """
        self.cent_nums = cent_nums
        self.atom_nums = atom_nums

        self.coords = coords
        # pass

    def __str__(self) -> str:
        """Produce string representation of class

        Returns:
            str: contents values of class as string
        """
        return f"{self.cent_nums}, {self.atom_nums}, {self.coords}"

    def __repr__(self):
        """Produce representation of class

        Returns:
            str: contents values of class as string
        """
        return str(self)

    def get_coords(self):
        """Returns the coordinates of the class

        Returns:
            float: list of the coordinates in [x,y,z] per atom
        """
        return self.coords

    def get_cent(self):
        """Returns the list of the centre number of the class

        Returns:
            int: List of the centre numbers in the class
        """
        return self.cent_nums

    def get_atom(self):
        """Returns the list of the atom number of the class

        Returns:
            int: List of the atomic numbers stored in the class
        """
        return self.atom_nums
