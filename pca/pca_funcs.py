"""
Functions to perform PCA on trajectory results

"""

from sklearn.decomposition import PCA


def pca_traj(geom, pca_comps=2):
    """Function to perform Principle Component Analysis on trajectory

    Args:
        geom (list of mol class): contains the frames of the trajectories
        num_sim (int):  Number of steps
        pca_comps (int): Number of components used in PCA
    """
    # https://mdtraj.org/1.9.4/examples/pca.html
    # for example applied to a MD trajectory

    pca1 = PCA(n_components=pca_comps)
    # need to take geom frames and pull out just coords
    # coords = []
    # for frame in geom:
    #     xyz = []
    #     # have to reshape list so 1D for each entry
    #     for x in frame.get_coords():
    #         for y in x:
    #             xyz.append(y)
    #     coords.append(xyz)

    # perform PCA fit
    print("Performing PCA fit, please wait...")
    reduced_cart = pca1.fit_transform(geom)

    return reduced_cart, pca1.components_, pca1.explained_variance_
