"""
Functions to work with geometries

"""

import math
import numpy as np
from sklearn.decomposition import PCA

# import matplotlib.pyplot as plt


def rmsd(coord_1, coord_2):
    """
    Return the RMSD between two sets of coordinates.

    Parameters
    ----------
    coord_1, coord_2 : list of x,y,z coordinates
        [x,y,z]
    Returns
    -------
    rmsd_out : float
        Root-Mean-Square Deviation
    """

    sum_squares = 0
    for atom_1, atom_2 in zip(coord_1, coord_2):
        # get positions of the first atom
        diff = (
            (atom_1[0] - atom_2[0]) * (atom_1[1] - atom_2[1]) * (atom_1[2] - atom_2[2])
        )
        sum_squares = sum_squares + (diff * diff)

    rmsd_out = math.sqrt(sum_squares / len(coord_1))

    return rmsd_out


def track_bond(coord_1, atom1, atom2):
    """Calculates the bond length between two atoms in the given structure
    Uses simple pythagoras

    Args:
        coord_1 (float): List of the coordinates
        atom1 (int): Centre number of atom 1
        atom2 (int): Centre number of atom 2

    Returns:
        bond_length (float): Distance between the two atoms
    """
    # Get distances of choosen bonds
    dist = 0
    for x in range(3):
        dist = dist + (coord_1[atom1 - 1][x] - coord_1[atom2 - 1][x]) * (
            coord_1[atom1 - 1][x] - coord_1[atom2 - 1][x]
        )

    bond_length = math.sqrt(dist)
    return bond_length


def track_angle(coord_1, atom1, atom2, atom3):
    """Calculates the angle between three atoms in the given structure
     Uses the dot product, is based on wiki formula
     is a bit unreliable as can flip sign randomly needs checks

    Args:
        coord_1 (float): List of the coordinates
        atom1 (int): Centre number of atom 1
        atom2 (int): Centre number of atom 2
        atom3 (int): Centre number of atom 3

    Returns:
       angle (float): The angle in degrees between the three atoms
    """
    # first get the two vectors
    vector_1 = []
    vector_2 = []
    for x in range(3):
        vector_1.append(coord_1[atom1 - 1][x] - coord_1[atom2 - 1][x])
        vector_2.append(coord_1[atom2 - 1][x] - coord_1[atom3 - 1][x])

    unit_vector_1 = vector_1 / np.linalg.norm(vector_1)
    unit_vector_2 = vector_2 / np.linalg.norm(vector_2)
    dot_product = np.dot(unit_vector_1, unit_vector_2)
    angle = np.arccos(dot_product)
    # convert to degrees from radians
    angle = angle * 57.2958

    return angle


def track_dihedral(coord_1, atom1, atom2, atom3, atom4):
    """Calculates the dihedral defined by the four atoms in the structure

    Args:
        coord_1 (float): List of the coordinates
        atom1 (int): Centre number of atom 1
        atom2 (int): Centre number of atom 2
        atom3 (int): Centre number of atom 3
        atom4 (int): Centre number of atom 4

    Returns:
        dihed(float): The dihedral angle in radians defined by the atoms
    """
    p0 = np.array(coord_1[atom1 - 1])
    p1 = np.array(coord_1[atom2 - 1])
    p2 = np.array(coord_1[atom3 - 1])
    p3 = np.array(coord_1[atom4 - 1])
    b0 = -1.0 * (p1 - p0)
    b1 = p2 - p1
    b2 = p3 - p2

    b0xb1 = np.cross(b0, b1)
    b1xb2 = np.cross(b2, b1)

    b0xb1_x_b1xb2 = np.cross(b0xb1, b1xb2)

    y = np.dot(b0xb1_x_b1xb2, b1) * (1.0 / np.linalg.norm(b1))
    x = np.dot(b0xb1, b1xb2)

    dihed = np.degrees(np.arctan2(y, x))

    return dihed


def sort_geoms(trajs_list):
    """Function to take all geoms from a list of trajectories and sort them
        into a 1D list of lists
    Args:
        geoms_list (list of trajectories): all the xyz coordinates from the trajectories
        file_list (list of the file number): file number the current geom came from
    Returns
        coords (list): All structures in 1D list. One list for each frame of all trajectories
        file_col (list): The trajectory/file number of the geometry
        index_col (list): The frame number within a trajectory of the geometry
    """

    coords = []  # the geometry
    file_col = []  # the trajectory number of the geometry
    index_col = []  # the frame number of the geometry
    files = 0
    for geo in trajs_list:
        files = files + 1
        ind = 0  # reset to zero
        for frame in geo:
            xyz = []
            ind = ind + 1
            # have to reshape list so 1D for each entry
            for x in frame.get_coords():
                for y in x:
                    xyz.append(y)
            coords.append(xyz)
            file_col.append(files)
            index_col.append(ind)

    return coords, file_col, index_col
