"""
Read in Trajectory files in xyz format suitable for jmol
Then perform analysis on them

Includes:
    Root Mean Square Deviation (RMSD)
"""

import numpy as np

import IO.io_funcs as iof
import geo.geom_funcs as gcf
import pca.pca_funcs as pcf
import frechet.frechet_funcs as frech
import clustering.dbscan_funcs as clust


def pca_run(pca_args):
    """Runs the PCA analysis code as a library import

    Args:
        pca_args (dict): dictionary with all the options

    """

    # useful variables here
    Save_Delim = "\t"
    Save_File = "output_"

    trajs = []
    filelist = []
    num_traj_list = []
    # Check if single file
    if pca_args["file"] is not None:
        # if is single file then
        # check that other options are not set
        if (pca_args["nfiles"] is not None) or (pca_args["dir"] is not None):
            print("Error, asking for more than one file")
            # Raise error and close programme here
        else:
            filename = pca_args["file"]
            print(f"Will load geometries from {filename}")
            numstr, trajs = iof.load_xyz(filename)
    else:
        # load more than one file
        Count = 0
        for i in range(pca_args["nfiles"]):
            # generate file name to load
            Count = i + 1
            cur_file = "ddtraj_" + str(i + 1) + ".pl"
            cur_file = iof.path.join(pca_args["dir"], cur_file)
            # load the file
            print(f"Will load geometries from {cur_file}")
            numstr, geom = iof.load_xyz(cur_file)
            # These will make sense as a trajectory class with properties
            trajs.append(geom)
            filelist.append(cur_file)
            num_traj_list.append(numstr)
            print(f"{numstr} geometries loaded from file {Count}")

    print(f"{numstr*Count} geometries loaded from {Count} files")

    if pca_args["save"]:
        # Check a file is given, if not use a default
        if pca_args["ofile"]:
            print(f"Files will be saved to {pca_args['ofile']}")
            Save_File = pca_args["ofile"]

    # generate the RMSD for each frame
    if pca_args["rmsd"]:
        # makes sense for each RMSD to be for a separate trajecory, not all
        # at least I think it does
        # Assume starting geometry is the reference
        rmsd_traj = []
        Count = 1
        # get each trajectory in list of trajectories
        for geo in trajs:
            # set up reference geometry
            comp_spec = geo[0]

            rmsd_list = []
            # get each frame of the geometry
            for frame in geo:
                rmsd_list.append(gcf.rmsd(comp_spec.get_coords(), frame.get_coords()))
            # save into big list
            rmsd_traj.append(rmsd_list)

            if pca_args["np"]:
                # plot RMSD solutions
                iof.plot_1d(
                    rmsd_list, "RMSD", "Simulation step", "RMSD / bohr", numstr, []
                )
            Count = Count + 1

        # save one file with all trajectories in
        if pca_args["save"]:
            # save to output
            rmsd_save = Save_File + "rmsd.txt"
            iof.save_1d(rmsd_save, np.arange(numstr), rmsd_traj, Save_Delim)

    if pca_args["bond"]:
        # makes sense for each bond to be for a separate trajectory, not all
        # at least I think it does
        at1 = pca_args["b1"]
        at2 = pca_args["b2"]
        bond_traj = []
        Count = 1
        for geo in trajs:
            # Calculate bond length
            # print(pcf.track_bond(geom[0].get_coords(), at1, at2))
            bond_list = []
            for frame in geo:
                bond_list.append(gcf.track_bond(frame.get_coords(), at1, at2))
            # save into big list
            bond_traj.append(bond_list)

            # plot Bond lengths
            if pca_args["np"]:
                iof.plot_1d(
                    bond_list,
                    "Bond Length",
                    "Simulation step",
                    f"Bond length atoms {at1}-{at2} / bohr",
                    numstr,
                    [],
                )
            Count = Count + 1
        if pca_args["save"]:
            # save to output
            bond_save = Save_File + "_bond.txt"
            iof.save_1d(bond_save, np.arange(numstr), bond_traj, Save_Delim)

    if pca_args["angle"]:
        # makes sense for each angle to be for a separate trajectory, not all
        # at least I think it does
        at1 = pca_args["a1"]
        at2 = pca_args["a2"]
        at3 = pca_args["a3"]
        angle_traj = []
        Count = 1

        for geo in trajs:
            angle_list = []
            for frame in geo:
                angle_list.append(gcf.track_angle(frame.get_coords(), at1, at2, at3))
            angle_traj.append(angle_list)

            # plot Angle
            if pca_args["np"]:
                iof.plot_1d(
                    angle_list,
                    "Bond Angle",
                    "Simulation step",
                    f"Bond angle atoms {at1}-{at2}-{at3} / degrees",
                    numstr,
                    [],
                )

            Count = Count + 1

        if pca_args["save"]:
            # save to output
            angle_save = Save_File + "_angle.txt"
            iof.save_1d(angle_save, np.arange(numstr), angle_traj, Save_Delim)
    # Calculate the dihedral angles
    if pca_args["dihed"]:
        at1 = pca_args["d1"]
        at2 = pca_args["d2"]
        at3 = pca_args["d3"]
        at4 = pca_args["d4"]
        dihed_traj = []
        Count = 1

        for geo in trajs:
            dihed_list = []
            for frame in geo:
                dihed_list.append(
                    gcf.track_dihedral(frame.get_coords(), at1, at2, at3, at4)
                )
            dihed_traj.append(dihed_list)
            # plot Angle
            if pca_args["np"]:
                iof.plot_1d(
                    dihed_list,
                    "Dihedral Angle",
                    "Simulation step",
                    f"Bond angle atoms {at1}-{at2}-{at3}--{at4} / degrees",
                    numstr,
                    [],
                )
            Count = Count + 1

        if pca_args["save"]:
            # save to output
            dihed_save = Save_File + "dihed.txt"
            iof.save_1d(dihed_save, np.arange(numstr), angle_traj, Save_Delim)

    # Do PCA calculation on geometries
    if pca_args["pca"]:
        # unlike above this will make sense to do pca on all traj at once
        # So will need to take all geoms and reshape into list of 1D lists
        # Must keep track of where we are in the array for later analysis
        # This info will be in the num_traj_list
        geo, file_cols, index_cols = gcf.sort_geoms(trajs)
        pca_res, pca_eigenfun, pca_eigenval = pcf.pca_traj(geo, pca_args["npca"])
        print("PCA Results")
        Count = 1
        pca_mol = []
        pca_xyz = []
        for func, val in zip(pca_eigenfun, pca_eigenval):
            print(f"PC{Count}: {val}")
            pca_mol.append(iof.pca_molclass(func, geom[0]))
            pca_xyz.append(iof.coords_pretty(pca_mol[Count - 1]))
            print(pca_xyz[Count - 1])
            Count = Count + 1

        # Plot the PCA results
        # Should be shown side by side
        if pca_args["np"]:
            iof.plot_1d(
                pca_res[:, 1].tolist(),
                "Cartesian coordinate PCA:\nColoured by trajectory number",
                "PC1",
                "PC2",
                numstr,
                xlist=pca_res[:, 0].tolist(),
                col=file_cols,
            )
            iof.plot_1d(
                pca_res[:, 1].tolist(),
                "Cartesian coordinate PCA:\nColoured by frame number",
                "PC1",
                "PC2",
                numstr,
                xlist=pca_res[:, 0].tolist(),
                col=index_cols,
            )
        if pca_args["save"]:
            # save to PCA results to output
            pca_save = Save_File + "_pca.txt"
            iof.save_1d(
                pca_save,
                pca_res[:, 0],
                [file_cols, index_cols, pca_res[:, 1]],
                Save_Delim,
            )
            # Save PCA components to output
            for index, comp in enumerate(pca_xyz):
                pca_save = Save_File + f"_pc{index}.pl"
                iof.save_xyzish(pca_save, comp)

    if pca_args["frech"]:
        # get geometries of all frames of all trajs in "geo"
        geo, file_cols, index_cols = gcf.sort_geoms(trajs)
        ntrajs = pca_args["nfiles"]
        xcoo = np.array(geo)
        # xcoo_split[m][n]: n = number of trajs, m = number of frames
        xcoo_split = np.split(xcoo, ntrajs)
        frames = len(xcoo_split[0])
        if (pca_args["t1"] is not None) and (pca_args["t2"] is not None):
            # get traj numbers
            traj_1 = pca_args["t1"]
            traj_2 = pca_args["t2"]
            print(
                "Calculating Fréchet dist. between traj {} and {} ...".format(
                    traj_1, traj_2
                )
            )

            dF = frech.Frechet(xcoo_split, ntrajs, frames, traj_1, traj_2)
            print(
                "Fréchet Distance between trajecotry {} and {} = {}".format(
                    traj_1, traj_2, dF
                ),
                "\n",
            )

        else:
            print(
                "\nCalculating Fréchet Distance Matrix between all pairs of {} trajectories ...".format(
                    ntrajs
                )
            )
            # initialise ntrajs x ntrajs Fréchet distance matrix
            F = [[0] * ntrajs for _ in range(ntrajs)]
            for i in range(ntrajs):
                for j in range(ntrajs):
                    F[i][j] = frech.Frechet(xcoo_split, ntrajs, frames, i + 1, j + 1)
            print("Done.\n")

            iof.write_frech(pca_args["dir"], F)

    if pca_args["dbscan"]:
        # Calculate Frechet distance matrix and write to file

        # get geometries of all frames of all trajs in "geo"
        geo, file_cols, index_cols = gcf.sort_geoms(trajs)
        ntrajs = pca_args["nfiles"]
        xcoo = np.array(geo)
        # xcoo_split[m][n]: n = number of trajs, m = number of frames
        xcoo_split = np.split(xcoo, ntrajs)
        frames = len(xcoo_split[0])

        print(
            "\nCalculating Fréchet Distance Matrix between all pairs of {} trajectories ...".format(
                ntrajs
            )
        )
        # initialise ntrajs x ntrajs Fréchet distance matrix
        F = [[0] * ntrajs for _ in range(ntrajs)]
        for i in range(ntrajs):
            for j in range(ntrajs):
                F[i][j] = frech.Frechet(xcoo_split, ntrajs, frames, i + 1, j + 1)
        print("Done.\n")

        iof.write_frech(pca_args["dir"], F)

        # Run clustering algorithm
        print("\nRunning DBSCAN Clustering Algorithm...\n")
        eps = pca_args["eps"]
        clust.dbscan_cluster(F, eps, pca_args["dir"], pca_args["nfiles"])

    if pca_args["dbscan_rd"]:
        print("\nRunning DBSCAN Clustering Algorithm on D_frechet.dat ...\n")
        eps = pca_args["eps"]
        F = iof.read_frech(pca_args["dir"])
        clust.dbscan_cluster(F, eps, pca_args["dir"], pca_args["nfiles"])


if __name__ == "__main__":
    print("DD trajectory analysis")
    # get the cli arguments
    parsey = iof.parsing()
    parsed_args = parsey.parse_known_args()
    pass_args = {
        "file": parsed_args[0].file,
        "nfiles": parsed_args[0].nfiles,
        "dir": parsed_args[0].dir,
        "save": parsed_args[0].save,
        "ofile": parsed_args[0].ofile,
        "rmsd": parsed_args[0].rmsd,
        "np": parsed_args[0].np,
        "bond": parsed_args[0].bond,
        "b1": parsed_args[0].b1,
        "b2": parsed_args[0].b2,
        "a1": parsed_args[0].a1,
        "a2": parsed_args[0].a2,
        "a3": parsed_args[0].a3,
        "dihed": parsed_args[0].dihed,
        "d1": parsed_args[0].d1,
        "d2": parsed_args[0].d2,
        "d3": parsed_args[0].d3,
        "d4": parsed_args[0].d4,
        "pca": parsed_args[0].pca,
        "angle": parsed_args[0].angle,
        "npca": parsed_args[0].npca,
        "frech": parsed_args[0].frech,
        "t1": parsed_args[0].t1,
        "t2": parsed_args[0].t2,
        "dbscan": parsed_args[0].dbscan,
        "dbscan_rd": parsed_args[0].dbscan_rd,
        "eps": parsed_args[0].eps,
    }
    # run analysis
    pca_run(pass_args)

    print("\nProgramme Complete")
