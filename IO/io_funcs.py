"""
Functions for input and output

Includes

load_xyz: Function to load xyz files into the programme
"""
import argparse as arg
import os.path as path

import numpy as np
import matplotlib.pyplot as plt


import classes.mol_class as mol


def parsing():
    """Function to read in CL arguments

    Returns:
        parser: the parsed arguments
    """
    parse = arg.ArgumentParser(
        description="Purpose: Analyse trajectory files from Direct Dynamics"
    )

    parse.add_argument(
        "-file",
        metavar="FILE",
        type=str,
        help="File name to read in trajectories from.",
    )
    parse.add_argument(
        "-dir",
        metavar="DIR",
        default="./",
        type=str,
        help="Folder in which data files will be found. \
            Can not be used with the -file option. \
                Requires number of files to be defined with -nifiles.",
    )
    parse.add_argument(
        "-nfiles",
        metavar="NFILES",
        type=int,
        help="How many trajectory files to use, can not be used with -file option. \
            Requires data directory to be defined with -dir",
    )
    parse.add_argument("-save", action="store_true", help="Save the results to file")
    parse.add_argument(
        "-ofile",
        metavar="OFILE",
        type=str,
        help="File to save output into. Give as file stem (without extension)",
    )
    parse.add_argument(
        "-rmsd",
        action="store_true",
        help="Calculates the RMSD between the initial structure and following structures",
    )
    parse.add_argument(
        "-bond",
        action="store_true",
        help="Calculates the bond length between two atoms. \
            If set then atom numbers must be given",
    )
    parse.add_argument(
        "--b1",
        metavar="B1",
        type=int,
        default=1,
        help="The atom of number of first atom in bond calculation, \
            required if bond is set to true",
    )
    parse.add_argument(
        "--b2",
        metavar="B2",
        type=int,
        default=2,
        help="The atom of number of second atom in bond calculation, \
            required if bond is set to true. Default is 1",
    )
    parse.add_argument(
        "-angle",
        action="store_true",
        help="Calculates the bond angle between three atoms. \
            If set then atom numbers must be given. Default is False",
    )

    parse.add_argument(
        "--a1",
        metavar="A1",
        type=int,
        default=1,
        help="The atom of number of first atom in angle calculation, \
            required if angle is set to true. Default is 1",
    )
    parse.add_argument(
        "--a2",
        metavar="A2",
        type=int,
        default=2,
        help="The atom of number of second atom in angle calculation, \
            required if angle is set to true. Default is 2",
    )
    parse.add_argument(
        "--a3",
        metavar="A3",
        type=int,
        default=3,
        help="The atom of number of third atom in angle calculation, \
            required if angle is set to true. Default is 3",
    )

    parse.add_argument(
        "-dihed",
        action="store_true",
        help="Calculates the dihreal angle defined by four atoms. \
            If set then atom numbers must be given. Default is False",
    )
    parse.add_argument(
        "--d1",
        metavar="D1",
        type=int,
        default=1,
        help="The atom of number of first atom in dihedrea calculation, \
            required if dihed is set to true. Default is 1",
    )
    parse.add_argument(
        "--d2",
        metavar="D2",
        type=int,
        default=2,
        help="The atom of number of second atom in dihedra; calculation, \
            required if dihed is set to true. Default is 2",
    )
    parse.add_argument(
        "--d3",
        metavar="D3",
        type=int,
        default=3,
        help="The atom of number of third atom in dihedral calculation, \
            required if dihed is set to true. Default is 3",
    )
    parse.add_argument(
        "--d4",
        metavar="D4",
        type=int,
        default=4,
        help="The atom of number of fourth atom in dihedral calculation, \
            required if dihed is set to true. Default is 4",
    )
    parse.add_argument(
        "-pca", action="store_true", help="Perform a PCA on the trajectories"
    )
    parse.add_argument(
        "--npca",
        metavar="NPCA",
        type=int,
        default=2,
        help="Number of components to use in the PCA. Default is 2",
    )
    parse.add_argument(
        "-np",
        action="store_false",
        help="Turns of plotting of results. Useful if want to run in script",
    )
    parse.add_argument(
        "-frech",
        action="store_true",
        help="Calculate Fréchet distance between trajectories",
    )
    parse.add_argument(
        "-t1",
        type=int,
        #        default=1,
        help="First trajectory in Fréchet analysis",
    )
    parse.add_argument(
        "-t2",
        type=int,
        #        default=2,
        help="Second trajectory in Fréchet analysis",
    )
    parse.add_argument(
        "-dbscan",
        action="store_true",
        help="Apply DBSCAN clustering algorithm to Fréchet analysis",
    )
    parse.add_argument(
        "-dbscan_rd",
        action="store_true",
        help="Apply DBSCAN clustering to already existing D_frechet.dat file",
    )
    parse.add_argument(
        "-eps",
        type=float,
        default=0.5,
        help="EPS value for DBSCAN (max. value between two samples)",
    )

    return parse


# def check_parse_options(parsed_args):

#     if


def load_xyz(filename):
    """This is a function to load xyz trajectory files

    Args:
        filename (string): file path to trajectory file, must exist
    """

    num_struc = 0  # number of structures found
    geom_list = []  # the geometry with time

    with open(filename, "r", encoding="utf-8") as inline:
        line = inline.readline()
        while line:
            if "Input orientation: " in line:
                # Found structure
                num_struc = num_struc + 1
                coord_list = []  # the geometry at a given time
                a_list = []  # the atom list at a given time
                cent = []  # centre numbers
                # Read on to structure proper
                for _ in range(5):
                    line = inline.readline()
                # next lines should contain geom
                while "-----------------" not in line:
                    struc = line.split()
                    # print(struc)
                    cent.append(int(struc[0]))
                    # get atom list
                    a_list.append(int(struc[1]))
                    # get coordinates of x,y,z
                    coord_list.append(
                        [float(struc[3]), float(struc[4]), float(struc[5])]
                    )
                    # coord_list.append(at.atom(struc[0], struc[1], struc[3], struc[4], struc[5]))
                    # print(geom)
                    line = inline.readline()
                # print(coord_list)
                mols = mol.Mol(cent, a_list, coord_list)
                geom_list.append(mols)
                # atom_list.append(a_list)
                # print(geom_list)
            line = inline.readline()
    # print(num_struc)
    return num_struc, geom_list


def save_1d(filename, x_data, y_data, delim):
    """Function to save out 1D data plot to file

    Args:
        filename (str): File name to save to
        x_data (float): X data to save out
        y_data (float): Y data to save out
        delim (str):    delimiter to use in file

    Returns:
       saved int: whether save worked or not 0 good
    """
    with open(filename, "w", encoding="utf-8") as writer:
        # Write to file
        for index, x in enumerate(x_data):
            # save_str = f"{x}{delim}{y}\n"
            save_str = f"{x}"
            for col in y_data:
                save_str = save_str + f"{delim}{col[index]}"
            save_str = save_str + "\n"
            writer.write(save_str)

    saved = 0

    return saved


def plot_1d(
    ylist, fig_cap, xlabel, ylabel, num_pnts, xlist=None, col=None, cbarcap="Int Scale"
):
    """Function to plot 1D data

    Args:
        ylist (float): List of the y values to plot
        fig_cap (str): Caption for figure
        xlabel (str): axis label
        ylabel (str): axis label
        num_pnts (int): how many points in list
        xlist (list, optional): x axis to plot against. Defaults to None.
        col (list, optional): colour scale. Defaults to None
        cbarcap (strong, optional): Name for the colour scale. Defaults to Int Scale
    """
    # plot data
    if not xlist:
        xlist = np.arange(num_pnts)

    _, ax = plt.subplots()
    if not col:
        ax.plot(xlist, ylist, "ro", markersize=3)
        ax.plot(xlist, ylist, "r-")
    else:
        ax.scatter(xlist, ylist, marker="x", c=col)
        # colour bar not working due to no mappable!
        # might need clever tricks to find children and hence mappable
        # pcm=ax.get_children()[2]
        # cbar = plt.colorbar(pcm, ax=ax)
        # cbar.set_label(cbarcap)

    ax.set_title(fig_cap)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)

    plt.show()


def pca_molclass(component, molexample):
    """Function to take 1D structure and resort into xyz

    Args:
        component (list of float): 1D list of the xyz components
        molexample (molclass): molecule with the numcent and atomcent properties

    Returns:
        mol_class: Mol_class object with PCA coordinates
    """
    pca_coords = []
    for index, value in enumerate(molexample.get_cent()):
        # get coordinates
        place = index * 3
        at_coord = [component[place], component[place + 1], component[place + 2]]
        pca_coords.append(at_coord)
    # create new PCA mol class object
    pca_mol = mol.Mol(molexample.get_cent(), molexample.get_atom(), pca_coords)

    return pca_mol


def coords_pretty(mol_example):
    """Function to take a mol class object and produce 'pretty' xyz

    Args:
        mol_example (mol_class): a mol_class object

    Returns:
        str: string of class suitable for printing or saving
    """

    nice_string = "\n Entering Gaussian System\n\n"
    nice_string = nice_string + "                          Input orientation:   \n"
    nice_string = (
        nice_string
        + " ---------------------------------------\
------------------------------\n"
    )
    nice_string = (
        nice_string
        + " Center     Atomic     Atomic            \
Coordinates (Angstroms)\n"
    )
    nice_string = (
        nice_string
        + " Number     Number      Type             \
X           Y           Z \n"
    )
    nice_string = (
        nice_string
        + "----------------------------------\
-----------------------------------\n"
    )

    for cent, atom, coord in zip(
        mol_example.get_cent(), mol_example.get_atom(), mol_example.get_coords()
    ):
        nice_string = nice_string + f"{cent}          {atom}             0        "
        for x in coord:
            nice_string = nice_string + f"{x:.6f}           "
        nice_string = nice_string + "\n"

    return nice_string


def save_xyzish(filename, xyzstring):
    """Save out xyz coordinates

    Args:
        filename (str): file path to save to
        xyzstring (str): String to write out
    """

    with open(filename, "w", encoding="utf-8") as outfile:
        outfile.write(xyzstring)


def write_frech(dirname, Matrix):
    """Save Frechet distance matrix

    Args:
        dirname (str): file path to save to
        Matrix (arr): Frechet distance matrix
    """

    with open(dirname + "D_frechet.dat", "w") as file:
        for i in Matrix:
            file.write("%s\n" % i)


def read_frech(dirname):
    """Read Frechet distance matrix from D_frechet.dat

    Args:
        dirname (str): file path to directory
    """

    # Define a function to parse each line into a list of floating-point numbers
    def parse_line(line):
        return [float(num) for num in line.strip("[]\n").split(", ")]

    # Read data from the file and parse each line
    F_rd = []
    with open(dirname + "D_frechet.dat", "r") as file:
        for line in file:
            F_rd.append(parse_line(line))

    return F_rd
