
 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.679721   -0.020178    0.000000
    2          8             0       -0.757223    0.122741    0.000000
    3          1             0        1.082683    0.987864    0.000000
    4          1             0        1.024117   -0.545246   -0.892224
    5          1             0        1.024117   -0.545246    0.892224
    6          1             0       -1.151456   -0.758236    0.000000
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.698558   -0.026247   -0.000276
    2          8             0       -0.763471    0.125602   -0.001087
    3          1             0        1.081597    0.968892    0.021888
    4          1             0        0.935597   -0.510067   -0.845409
    5          1             0        0.973581   -0.487038    0.844411
    6          1             0       -1.137123   -0.806025   -0.000535
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.705708   -0.019017   -0.003284
    2          8             0       -0.766841    0.132150   -0.000781
    3          1             0        1.083996    0.973578    0.030359
    4          1             0        0.911770   -0.554413   -0.867249
    5          1             0        0.954403   -0.534148    0.888613
    6          1             0       -1.127141   -0.909394   -0.000589
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.712215   -0.004958   -0.003311
    2          8             0       -0.771889    0.140303   -0.001390
    3          1             0        1.095756    0.985160    0.024351
    4          1             0        0.880502   -0.634433   -0.913502
    5          1             0        0.941880   -0.631805    0.950655
    6          1             0       -1.091001   -1.040262   -0.000571
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.719594    0.008195    0.000740
    2          8             0       -0.775562    0.152142   -0.002465
    3          1             0        1.120921    1.002977    0.004197
    4          1             0        0.817801   -0.709440   -0.946581
    5          1             0        0.892939   -0.725871    0.972607
    6          1             0       -1.032800   -1.233777   -0.000439
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.721809    0.009593    0.001978
    2          8             0       -0.777362    0.169598   -0.001822
    3          1             0        1.161841    1.000631   -0.016271
    4          1             0        0.781608   -0.720334   -0.883041
    5          1             0        0.828674   -0.755853    0.904338
    6          1             0       -0.971315   -1.484309   -0.000009
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.720564    0.003427    0.002574
    2          8             0       -0.778911    0.190838   -0.000568
    3          1             0        1.239550    0.970146   -0.035898
    4          1             0        0.770617   -0.675785   -0.780182
    5          1             0        0.773249   -0.726279    0.793877
    6          1             0       -0.944310   -1.791670    0.000505
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.714546   -0.002558    0.003318
    2          8             0       -0.779853    0.214552    0.000659
    3          1             0        1.323012    0.911062   -0.052631
    4          1             0        0.807576   -0.624520   -0.763179
    5          1             0        0.766266   -0.684289    0.765114
    6          1             0       -0.971278   -2.131296    0.000962
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.706014   -0.004703    0.005252
    2          8             0       -0.781285    0.239319    0.001337
    3          1             0        1.391326    0.834199   -0.045270
    4          1             0        0.877958   -0.615452   -0.867916
    5          1             0        0.807337   -0.641185    0.829154
    6          1             0       -1.025417   -2.474861    0.000898
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.700077   -0.006679    0.004300
    2          8             0       -0.781741    0.255716    0.002118
    3          1             0        1.422391    0.786953   -0.024605
    4          1             0        0.930993   -0.620342   -0.957145
    5          1             0        0.842142   -0.608027    0.897102
    6          1             0       -1.065179   -2.693085    0.000663
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.700077   -0.006679    0.004300
    2          8             0       -0.781741    0.255716    0.002118
    3          1             0        1.422391    0.786953   -0.024605
    4          1             0        0.930993   -0.620342   -0.957145
    5          1             0        0.842142   -0.608027    0.897102
    6          1             0       -1.065179   -2.693085    0.000663
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.700065   -0.006746    0.004210
    2          8             0       -0.781715    0.255708    0.002130
    3          1             0        1.422352    0.787464   -0.024518
    4          1             0        0.930889   -0.620064   -0.956455
    5          1             0        0.842058   -0.608078    0.897189
    6          1             0       -1.065213   -2.692908    0.000665
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.700062   -0.006760    0.004191
    2          8             0       -0.781710    0.255706    0.002133
    3          1             0        1.422345    0.787572   -0.024498
    4          1             0        0.930868   -0.620005   -0.956307
    5          1             0        0.842040   -0.608088    0.897207
    6          1             0       -1.065221   -2.692871    0.000665
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.700062   -0.006760    0.004191
    2          8             0       -0.781710    0.255706    0.002133
    3          1             0        1.422345    0.787572   -0.024498
    4          1             0        0.930868   -0.620005   -0.956307
    5          1             0        0.842040   -0.608088    0.897207
    6          1             0       -1.065221   -2.692871    0.000665
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.699933   -0.007652    0.003149
    2          8             0       -0.781235    0.255705    0.002201
    3          1             0        1.420122    0.795610   -0.023654
    4          1             0        0.927899   -0.616954   -0.947122
    5          1             0        0.841648   -0.610031    0.898407
    6          1             0       -1.065633   -2.691299    0.000678
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.699085   -0.013005   -0.002677
    2          8             0       -0.777683    0.255848    0.002463
    3          1             0        1.398702    0.844928   -0.017838
    4          1             0        0.907143   -0.601976   -0.893872
    5          1             0        0.839266   -0.622507    0.904062
    6          1             0       -1.067295   -2.681248    0.000710
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.697507   -0.018811   -0.008570
    2          8             0       -0.770870    0.256052    0.002807
    3          1             0        1.344237    0.900182   -0.003379
    4          1             0        0.881742   -0.600389   -0.843461
    5          1             0        0.830864   -0.633015    0.903605
    6          1             0       -1.068006   -2.661248    0.000604
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.697755   -0.019183   -0.007881
    2          8             0       -0.768440    0.255889    0.002839
    3          1             0        1.313699    0.906236    0.003089
    4          1             0        0.880019   -0.613893   -0.852345
    5          1             0        0.825746   -0.630227    0.897537
    6          1             0       -1.071865   -2.649513    0.000483
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.698363   -0.017313   -0.003018
    2          8             0       -0.766110    0.255111    0.002762
    3          1             0        1.270971    0.893977    0.008381
    4          1             0        0.894995   -0.644839   -0.897696
    5          1             0        0.822005   -0.619152    0.881572
    6          1             0       -1.084093   -2.627348    0.000299
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.699420   -0.017875   -0.001856
    2          8             0       -0.765419    0.254666    0.002822
    3          1             0        1.246945    0.898621    0.009152
    4          1             0        0.901932   -0.653824   -0.909340
    5          1             0        0.821923   -0.618020    0.877800
    6          1             0       -1.090248   -2.610294    0.000275
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.700579   -0.019474   -0.003397
    2          8             0       -0.766109    0.254592    0.002961
    3          1             0        1.244067    0.910282    0.006510
    4          1             0        0.900412   -0.645942   -0.895289
    5          1             0        0.822615   -0.623781    0.882322
    6          1             0       -1.089434   -2.603736    0.000366
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.701843   -0.021574   -0.005333
    2          8             0       -0.766822    0.254156    0.003244
    3          1             0        1.237499    0.924458    0.002447
    4          1             0        0.901611   -0.635790   -0.876271
    5          1             0        0.823059   -0.630773    0.885645
    6          1             0       -1.088315   -2.588967    0.000510
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.702388   -0.022207   -0.005858
    2          8             0       -0.766969    0.253973    0.003465
    3          1             0        1.230894    0.928133    0.002368
    4          1             0        0.904925   -0.635566   -0.873425
    5          1             0        0.821738   -0.631763    0.885597
    6          1             0       -1.087846   -2.581378    0.000552
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.702798   -0.022530   -0.006057
    2          8             0       -0.767106    0.253928    0.003602
    3          1             0        1.226642    0.930006    0.002944
    4          1             0        0.907150   -0.636721   -0.873702
    5          1             0        0.820570   -0.631707    0.885494
    6          1             0       -1.087327   -2.577540    0.000565
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.702798   -0.022530   -0.006057
    2          8             0       -0.767106    0.253928    0.003602
    3          1             0        1.226642    0.930006    0.002944
    4          1             0        0.907150   -0.636721   -0.873702
    5          1             0        0.820570   -0.631707    0.885494
    6          1             0       -1.087327   -2.577540    0.000565
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.702798   -0.022530   -0.006057
    2          8             0       -0.767106    0.253928    0.003602
    3          1             0        1.226642    0.930006    0.002944
    4          1             0        0.907150   -0.636721   -0.873702
    5          1             0        0.820570   -0.631707    0.885494
    6          1             0       -1.087327   -2.577540    0.000565
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.702798   -0.022530   -0.006057
    2          8             0       -0.767106    0.253928    0.003602
    3          1             0        1.226642    0.930006    0.002944
    4          1             0        0.907150   -0.636721   -0.873702
    5          1             0        0.820570   -0.631707    0.885494
    6          1             0       -1.087327   -2.577540    0.000565
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.702798   -0.022530   -0.006057
    2          8             0       -0.767106    0.253928    0.003602
    3          1             0        1.226642    0.930006    0.002944
    4          1             0        0.907150   -0.636721   -0.873702
    5          1             0        0.820570   -0.631707    0.885494
    6          1             0       -1.087327   -2.577540    0.000565
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.702798   -0.022530   -0.006057
    2          8             0       -0.767106    0.253928    0.003602
    3          1             0        1.226642    0.930006    0.002944
    4          1             0        0.907150   -0.636721   -0.873702
    5          1             0        0.820570   -0.631707    0.885494
    6          1             0       -1.087327   -2.577540    0.000565
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.702798   -0.022530   -0.006057
    2          8             0       -0.767106    0.253928    0.003602
    3          1             0        1.226642    0.930006    0.002944
    4          1             0        0.907150   -0.636721   -0.873702
    5          1             0        0.820570   -0.631707    0.885494
    6          1             0       -1.087327   -2.577540    0.000565
 ---------------------------------------------------------------------
