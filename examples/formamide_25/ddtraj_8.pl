
 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.000000    0.401721   -0.096031
    2          7             0        0.000000   -0.162504    1.145371
    3          1             0        0.000000    1.541283   -0.009698
    4          1             0        0.000000    0.374796    1.979555
    5          8             0        0.000000   -0.207339   -1.131411
    6          1             0        0.000000   -1.153754    1.217289
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.007990    0.422308   -0.068235
    2          7             0        0.004240   -0.168752    1.121734
    3          1             0        0.031230    1.502564   -0.019653
    4          1             0        0.051788    0.387783    1.978621
    5          8             0       -0.002509   -0.214575   -1.142130
    6          1             0       -0.006959   -1.171503    1.395755
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.005714    0.438110   -0.090635
    2          7             0       -0.005755   -0.195409    1.136923
    3          1             0        0.033483    1.440808   -0.181797
    4          1             0        0.118852    0.602788    2.211482
    5          8             0       -0.002448   -0.212594   -1.123040
    6          1             0        0.034536   -1.173959    1.077757
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.003255    0.484423   -0.042841
    2          7             0        0.002734   -0.206442    1.100803
    3          1             0       -0.075422    1.466506   -0.301844
    4          1             0        0.049790    0.543520    2.300333
    5          8             0       -0.002588   -0.231800   -1.136354
    6          1             0       -0.010028   -1.233728    1.253061
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.004277    0.482348   -0.027987
    2          7             0        0.005269   -0.230158    1.089266
    3          1             0       -0.086929    1.564803   -0.465025
    4          1             0        0.011130    0.660388    2.481115
    5          8             0        0.005429   -0.222727   -1.133207
    6          1             0       -0.134501   -1.238655    1.168983
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.000869    0.505279   -0.024029
    2          7             0        0.013212   -0.252909    1.084030
    3          1             0       -0.094033    1.618722   -0.513537
    4          1             0       -0.009339    0.710931    2.551064
    5          8             0        0.007236   -0.230217   -1.131699
    6          1             0       -0.184701   -1.181187    1.149256
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.005035    0.528549   -0.047521
    2          7             0        0.023424   -0.262759    1.081957
    3          1             0       -0.113770    1.524496   -0.479719
    4          1             0       -0.041865    0.722475    2.681996
    5          8             0        0.011529   -0.234594   -1.120224
    6          1             0       -0.292837   -1.169242    1.110898
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.007477    0.541314   -0.100490
    2          7             0        0.040439   -0.264798    1.087174
    3          1             0       -0.164460    1.416592   -0.387101
    4          1             0       -0.110286    0.646548    2.885575
    5          8             0        0.022158   -0.232843   -1.097028
    6          1             0       -0.549766   -1.136848    1.004788
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.013424    0.535487   -0.166484
    2          7             0        0.073120   -0.260969    1.105305
    3          1             0       -0.216618    1.551385   -0.265969
    4          1             0       -0.245102    0.391022    3.047609
    5          8             0        0.039592   -0.226032   -1.070501
    6          1             0       -1.022794   -1.108033    0.834503
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.012398    0.483980   -0.249743
    2          7             0        0.082447   -0.256747    1.127586
    3          1             0        0.068198    2.124279   -0.073864
    4          1             0       -0.447030   -0.077221    3.209255
    5          8             0        0.056171   -0.192174   -1.039187
    6          1             0       -1.510627   -1.195393    0.665547
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.006932    0.396376   -0.371988
    2          7             0        0.029500   -0.242543    1.174830
    3          1             0        0.909508    2.682279    0.286421
    4          1             0       -0.513153   -0.752991    3.246991
    5          8             0        0.068275   -0.123367   -1.000257
    6          1             0       -1.807274   -1.323854    0.448798
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.005276    0.392728   -0.379105
    2          7             0        0.023052   -0.240988    1.180436
    3          1             0        0.968268    2.683338    0.313815
    4          1             0       -0.504186   -0.801807    3.220693
    5          8             0        0.069172   -0.118704   -0.998861
    6          1             0       -1.819365   -1.328273    0.432395
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.005249    0.392866   -0.379013
    2          7             0        0.023020   -0.240974    1.180525
    3          1             0        0.967840    2.681793    0.313754
    4          1             0       -0.504093   -0.801310    3.219508
    5          8             0        0.069176   -0.118739   -0.998924
    6          1             0       -1.818970   -1.328517    0.432315
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.005204    0.393096   -0.378860
    2          7             0        0.022965   -0.240950    1.180673
    3          1             0        0.967126    2.679218    0.313651
    4          1             0       -0.503939   -0.800480    3.217534
    5          8             0        0.069184   -0.118797   -0.999029
    6          1             0       -1.818311   -1.328921    0.432182
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.005160    0.393320   -0.378710
    2          7             0        0.022912   -0.240927    1.180816
    3          1             0        0.966432    2.676712    0.313550
    4          1             0       -0.503790   -0.799671    3.215614
    5          8             0        0.069191   -0.118853   -0.999132
    6          1             0       -1.817669   -1.329314    0.432054
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.005131    0.393470   -0.378610
    2          7             0        0.022877   -0.240912    1.180913
    3          1             0        0.965966    2.675028    0.313483
    4          1             0       -0.503689   -0.799126    3.214323
    5          8             0        0.069196   -0.118891   -0.999201
    6          1             0       -1.817237   -1.329578    0.431968
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.005131    0.393470   -0.378610
    2          7             0        0.022877   -0.240912    1.180913
    3          1             0        0.965966    2.675028    0.313483
    4          1             0       -0.503689   -0.799126    3.214323
    5          8             0        0.069196   -0.118891   -0.999201
    6          1             0       -1.817237   -1.329578    0.431968
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.005131    0.393470   -0.378610
    2          7             0        0.022877   -0.240912    1.180913
    3          1             0        0.965966    2.675028    0.313483
    4          1             0       -0.503689   -0.799126    3.214323
    5          8             0        0.069196   -0.118891   -0.999201
    6          1             0       -1.817237   -1.329578    0.431968
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.005131    0.393470   -0.378610
    2          7             0        0.022877   -0.240912    1.180913
    3          1             0        0.965966    2.675028    0.313483
    4          1             0       -0.503689   -0.799126    3.214323
    5          8             0        0.069196   -0.118891   -0.999201
    6          1             0       -1.817237   -1.329578    0.431968
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.005131    0.393470   -0.378610
    2          7             0        0.022877   -0.240912    1.180913
    3          1             0        0.965966    2.675028    0.313483
    4          1             0       -0.503689   -0.799126    3.214323
    5          8             0        0.069196   -0.118891   -0.999201
    6          1             0       -1.817237   -1.329578    0.431968
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.005131    0.393470   -0.378610
    2          7             0        0.022877   -0.240912    1.180913
    3          1             0        0.965966    2.675028    0.313483
    4          1             0       -0.503689   -0.799126    3.214323
    5          8             0        0.069196   -0.118891   -0.999201
    6          1             0       -1.817237   -1.329578    0.431968
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.005131    0.393470   -0.378610
    2          7             0        0.022877   -0.240912    1.180913
    3          1             0        0.965966    2.675028    0.313483
    4          1             0       -0.503689   -0.799126    3.214323
    5          8             0        0.069196   -0.118891   -0.999201
    6          1             0       -1.817237   -1.329578    0.431968
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.005131    0.393470   -0.378610
    2          7             0        0.022877   -0.240912    1.180913
    3          1             0        0.965966    2.675028    0.313483
    4          1             0       -0.503689   -0.799126    3.214323
    5          8             0        0.069196   -0.118891   -0.999201
    6          1             0       -1.817237   -1.329578    0.431968
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.005131    0.393470   -0.378610
    2          7             0        0.022877   -0.240912    1.180913
    3          1             0        0.965966    2.675028    0.313483
    4          1             0       -0.503689   -0.799126    3.214323
    5          8             0        0.069196   -0.118891   -0.999201
    6          1             0       -1.817237   -1.329578    0.431968
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.005131    0.393470   -0.378610
    2          7             0        0.022877   -0.240912    1.180913
    3          1             0        0.965966    2.675028    0.313483
    4          1             0       -0.503689   -0.799126    3.214323
    5          8             0        0.069196   -0.118891   -0.999201
    6          1             0       -1.817237   -1.329578    0.431968
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.005131    0.393470   -0.378610
    2          7             0        0.022877   -0.240912    1.180913
    3          1             0        0.965966    2.675028    0.313483
    4          1             0       -0.503689   -0.799126    3.214323
    5          8             0        0.069196   -0.118891   -0.999201
    6          1             0       -1.817237   -1.329578    0.431968
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.005131    0.393470   -0.378610
    2          7             0        0.022877   -0.240912    1.180913
    3          1             0        0.965966    2.675028    0.313483
    4          1             0       -0.503689   -0.799126    3.214323
    5          8             0        0.069196   -0.118891   -0.999201
    6          1             0       -1.817237   -1.329578    0.431968
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.005131    0.393470   -0.378610
    2          7             0        0.022877   -0.240912    1.180913
    3          1             0        0.965966    2.675028    0.313483
    4          1             0       -0.503689   -0.799126    3.214323
    5          8             0        0.069196   -0.118891   -0.999201
    6          1             0       -1.817237   -1.329578    0.431968
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.005131    0.393470   -0.378610
    2          7             0        0.022877   -0.240912    1.180913
    3          1             0        0.965966    2.675028    0.313483
    4          1             0       -0.503689   -0.799126    3.214323
    5          8             0        0.069196   -0.118891   -0.999201
    6          1             0       -1.817237   -1.329578    0.431968
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.005131    0.393470   -0.378610
    2          7             0        0.022877   -0.240912    1.180913
    3          1             0        0.965966    2.675028    0.313483
    4          1             0       -0.503689   -0.799126    3.214323
    5          8             0        0.069196   -0.118891   -0.999201
    6          1             0       -1.817237   -1.329578    0.431968
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.005131    0.393470   -0.378610
    2          7             0        0.022877   -0.240912    1.180913
    3          1             0        0.965966    2.675028    0.313483
    4          1             0       -0.503689   -0.799126    3.214323
    5          8             0        0.069196   -0.118891   -0.999201
    6          1             0       -1.817237   -1.329578    0.431968
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.005131    0.393470   -0.378610
    2          7             0        0.022877   -0.240912    1.180913
    3          1             0        0.965966    2.675028    0.313483
    4          1             0       -0.503689   -0.799126    3.214323
    5          8             0        0.069196   -0.118891   -0.999201
    6          1             0       -1.817237   -1.329578    0.431968
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.005131    0.393470   -0.378610
    2          7             0        0.022877   -0.240912    1.180913
    3          1             0        0.965966    2.675028    0.313483
    4          1             0       -0.503689   -0.799126    3.214323
    5          8             0        0.069196   -0.118891   -0.999201
    6          1             0       -1.817237   -1.329578    0.431968
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.005131    0.393470   -0.378610
    2          7             0        0.022877   -0.240912    1.180913
    3          1             0        0.965966    2.675028    0.313483
    4          1             0       -0.503689   -0.799126    3.214323
    5          8             0        0.069196   -0.118891   -0.999201
    6          1             0       -1.817237   -1.329578    0.431968
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.005131    0.393470   -0.378610
    2          7             0        0.022877   -0.240912    1.180913
    3          1             0        0.965966    2.675028    0.313483
    4          1             0       -0.503689   -0.799126    3.214323
    5          8             0        0.069196   -0.118891   -0.999201
    6          1             0       -1.817237   -1.329578    0.431968
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.005131    0.393470   -0.378610
    2          7             0        0.022877   -0.240912    1.180913
    3          1             0        0.965966    2.675028    0.313483
    4          1             0       -0.503689   -0.799126    3.214323
    5          8             0        0.069196   -0.118891   -0.999201
    6          1             0       -1.817237   -1.329578    0.431968
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0       -0.005131    0.393470   -0.378610
    2          7             0        0.022877   -0.240912    1.180913
    3          1             0        0.965966    2.675028    0.313483
    4          1             0       -0.503689   -0.799126    3.214323
    5          8             0        0.069196   -0.118891   -0.999201
    6          1             0       -1.817237   -1.329578    0.431968
 ---------------------------------------------------------------------
