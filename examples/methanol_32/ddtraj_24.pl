
 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.679721   -0.020178    0.000000
    2          8             0       -0.757223    0.122741    0.000000
    3          1             0        1.082683    0.987864    0.000000
    4          1             0        1.024117   -0.545246   -0.892224
    5          1             0        1.024117   -0.545246    0.892224
    6          1             0       -1.151456   -0.758236    0.000000
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.650127   -0.016493   -0.002197
    2          8             0       -0.738998    0.124722    0.000923
    3          1             0        1.108118    0.979148    0.009707
    4          1             0        1.029644   -0.559442   -0.904052
    5          1             0        1.011285   -0.547236    0.905922
    6          1             0       -1.106376   -0.809387   -0.000001
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.626039   -0.011557   -0.001576
    2          8             0       -0.726704    0.129511    0.000691
    3          1             0        1.138171    0.982628    0.001444
    4          1             0        1.037590   -0.582475   -0.895304
    5          1             0        1.022927   -0.582883    0.901595
    6          1             0       -1.064248   -0.889260    0.000100
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.600179   -0.015298   -0.006021
    2          8             0       -0.716826    0.137586    0.001130
    3          1             0        1.170051    0.954100    0.012639
    4          1             0        1.077558   -0.560413   -0.868301
    5          1             0        1.070084   -0.567353    0.909164
    6          1             0       -1.032569   -0.982249    0.000040
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.602646   -0.009915   -0.002290
    2          8             0       -0.711907    0.144521    0.000403
    3          1             0        1.143852    0.937886   -0.009021
    4          1             0        1.041666   -0.572144   -0.822675
    5          1             0        1.038168   -0.598873    0.852140
    6          1             0       -1.045689   -1.096916    0.000254
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.618477   -0.002388    0.001987
    2          8             0       -0.707173    0.152293   -0.000566
    3          1             0        1.125411    0.968806   -0.018958
    4          1             0        0.951470   -0.606379   -0.810910
    5          1             0        0.958470   -0.631782    0.814924
    6          1             0       -1.119542   -1.273554    0.000218
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.640626    0.004875    0.004705
    2          8             0       -0.702831    0.164590   -0.001655
    3          1             0        1.116172    1.014758   -0.017296
    4          1             0        0.845988   -0.643258   -0.853348
    5          1             0        0.875464   -0.659618    0.840839
    6          1             0       -1.251734   -1.536456   -0.000012
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.662835    0.012848    0.006791
    2          8             0       -0.701093    0.179252   -0.002739
    3          1             0        1.126475    1.022115   -0.018384
    4          1             0        0.759724   -0.661074   -0.917629
    5          1             0        0.814053   -0.677458    0.898663
    6          1             0       -1.403566   -1.836088   -0.000183
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.681749    0.014358    0.008249
    2          8             0       -0.702346    0.195889   -0.003565
    3          1             0        1.159217    0.998533   -0.027357
    4          1             0        0.708423   -0.629818   -0.948003
    5          1             0        0.781405   -0.661146    0.933631
    6          1             0       -1.555854   -2.142335   -0.000168
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.697069    0.005486    0.007615
    2          8             0       -0.707632    0.214548   -0.003431
    3          1             0        1.209986    0.962498   -0.033757
    4          1             0        0.714151   -0.551592   -0.924882
    5          1             0        0.785048   -0.596780    0.922142
    6          1             0       -1.713915   -2.439198   -0.000026
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.708935   -0.006789    0.009888
    2          8             0       -0.716885    0.235259   -0.003538
    3          1             0        1.272872    0.926464   -0.028327
    4          1             0        0.767359   -0.488277   -0.899262
    5          1             0        0.832059   -0.509559    0.866078
    6          1             0       -1.871434   -2.735590   -0.000174
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.710447   -0.008277    0.010780
    2          8             0       -0.718434    0.238339   -0.003696
    3          1             0        1.279830    0.921928   -0.026309
    4          1             0        0.777497   -0.484814   -0.900182
    5          1             0        0.843431   -0.498272    0.857004
    6          1             0       -1.893266   -2.776847   -0.000247
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.710454   -0.007972    0.011102
    2          8             0       -0.718598    0.238517   -0.003783
    3          1             0        1.277911    0.922766   -0.025470
    4          1             0        0.779020   -0.489433   -0.903966
    5          1             0        0.846361   -0.500224    0.857539
    6          1             0       -1.893216   -2.777564   -0.000280
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.710442   -0.007304    0.011748
    2          8             0       -0.718943    0.238906   -0.003970
    3          1             0        1.273822    0.924599   -0.023870
    4          1             0        0.782162   -0.499120   -0.911651
    5          1             0        0.852710   -0.504851    0.858989
    6          1             0       -1.892878   -2.779152   -0.000345
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.710033   -0.006013    0.011531
    2          8             0       -0.719462    0.239210   -0.004128
    3          1             0        1.270866    0.923737   -0.024330
    4          1             0        0.786063   -0.506515   -0.915338
    5          1             0        0.862542   -0.516402    0.868191
    6          1             0       -1.890434   -2.779514   -0.000356
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.710033   -0.006013    0.011531
    2          8             0       -0.719462    0.239210   -0.004128
    3          1             0        1.270866    0.923737   -0.024330
    4          1             0        0.786063   -0.506515   -0.915338
    5          1             0        0.862542   -0.516402    0.868191
    6          1             0       -1.890434   -2.779514   -0.000356
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.710033   -0.006013    0.011531
    2          8             0       -0.719462    0.239210   -0.004128
    3          1             0        1.270866    0.923737   -0.024330
    4          1             0        0.786063   -0.506515   -0.915338
    5          1             0        0.862542   -0.516402    0.868191
    6          1             0       -1.890434   -2.779514   -0.000356
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.710033   -0.006013    0.011531
    2          8             0       -0.719462    0.239210   -0.004128
    3          1             0        1.270866    0.923737   -0.024330
    4          1             0        0.786063   -0.506515   -0.915338
    5          1             0        0.862542   -0.516402    0.868191
    6          1             0       -1.890434   -2.779514   -0.000356
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.710033   -0.006013    0.011531
    2          8             0       -0.719462    0.239210   -0.004128
    3          1             0        1.270866    0.923737   -0.024330
    4          1             0        0.786063   -0.506515   -0.915338
    5          1             0        0.862542   -0.516402    0.868191
    6          1             0       -1.890434   -2.779514   -0.000356
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.710033   -0.006013    0.011531
    2          8             0       -0.719462    0.239210   -0.004128
    3          1             0        1.270866    0.923737   -0.024330
    4          1             0        0.786063   -0.506515   -0.915338
    5          1             0        0.862542   -0.516402    0.868191
    6          1             0       -1.890434   -2.779514   -0.000356
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.710033   -0.006013    0.011531
    2          8             0       -0.719462    0.239210   -0.004128
    3          1             0        1.270866    0.923737   -0.024330
    4          1             0        0.786063   -0.506515   -0.915338
    5          1             0        0.862542   -0.516402    0.868191
    6          1             0       -1.890434   -2.779514   -0.000356
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.710033   -0.006013    0.011531
    2          8             0       -0.719462    0.239210   -0.004128
    3          1             0        1.270866    0.923737   -0.024330
    4          1             0        0.786063   -0.506515   -0.915338
    5          1             0        0.862542   -0.516402    0.868191
    6          1             0       -1.890434   -2.779514   -0.000356
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.710033   -0.006013    0.011531
    2          8             0       -0.719462    0.239210   -0.004128
    3          1             0        1.270866    0.923737   -0.024330
    4          1             0        0.786063   -0.506515   -0.915338
    5          1             0        0.862542   -0.516402    0.868191
    6          1             0       -1.890434   -2.779514   -0.000356
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.710033   -0.006013    0.011531
    2          8             0       -0.719462    0.239210   -0.004128
    3          1             0        1.270866    0.923737   -0.024330
    4          1             0        0.786063   -0.506515   -0.915338
    5          1             0        0.862542   -0.516402    0.868191
    6          1             0       -1.890434   -2.779514   -0.000356
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.710033   -0.006013    0.011531
    2          8             0       -0.719462    0.239210   -0.004128
    3          1             0        1.270866    0.923737   -0.024330
    4          1             0        0.786063   -0.506515   -0.915338
    5          1             0        0.862542   -0.516402    0.868191
    6          1             0       -1.890434   -2.779514   -0.000356
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.710033   -0.006013    0.011531
    2          8             0       -0.719462    0.239210   -0.004128
    3          1             0        1.270866    0.923737   -0.024330
    4          1             0        0.786063   -0.506515   -0.915338
    5          1             0        0.862542   -0.516402    0.868191
    6          1             0       -1.890434   -2.779514   -0.000356
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.710033   -0.006013    0.011531
    2          8             0       -0.719462    0.239210   -0.004128
    3          1             0        1.270866    0.923737   -0.024330
    4          1             0        0.786063   -0.506515   -0.915338
    5          1             0        0.862542   -0.516402    0.868191
    6          1             0       -1.890434   -2.779514   -0.000356
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.710033   -0.006013    0.011531
    2          8             0       -0.719462    0.239210   -0.004128
    3          1             0        1.270866    0.923737   -0.024330
    4          1             0        0.786063   -0.506515   -0.915338
    5          1             0        0.862542   -0.516402    0.868191
    6          1             0       -1.890434   -2.779514   -0.000356
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.710033   -0.006013    0.011531
    2          8             0       -0.719462    0.239210   -0.004128
    3          1             0        1.270866    0.923737   -0.024330
    4          1             0        0.786063   -0.506515   -0.915338
    5          1             0        0.862542   -0.516402    0.868191
    6          1             0       -1.890434   -2.779514   -0.000356
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.710033   -0.006013    0.011531
    2          8             0       -0.719462    0.239210   -0.004128
    3          1             0        1.270866    0.923737   -0.024330
    4          1             0        0.786063   -0.506515   -0.915338
    5          1             0        0.862542   -0.516402    0.868191
    6          1             0       -1.890434   -2.779514   -0.000356
 ---------------------------------------------------------------------
