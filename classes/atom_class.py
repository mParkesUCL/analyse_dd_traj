"""
A class for atoms
"""


class atom:
    def __init__(self, cent_num, atom_num, x, y, z) -> None:
        """_summary_

        Args:
            cent_num (_type_): _description_
            atom_num (_type_): _description_
            x (_type_): _description_
            y (_type_): _description_
            z (_type_): _description_
        """
        self.cent_num = int(cent_num)
        self.atom_num = int(atom_num)

        self.coords = [float(x), float(y), float(z)]
        pass

    def __str__(self) -> str:
        return f"{self.cent_num}, {self.atom_num}, {self.coords}"

    def __repr__(self):
        return str(self)

    def get_coords(self):
        return self.coords

    def get_cent(self):
        return self.cent_num

    def get_atom(self):
        return self.atom_num
