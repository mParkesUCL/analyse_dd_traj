
 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.679721   -0.020178    0.000000
    2          8             0       -0.757223    0.122741    0.000000
    3          1             0        1.082683    0.987864    0.000000
    4          1             0        1.024117   -0.545246   -0.892224
    5          1             0        1.024117   -0.545246    0.892224
    6          1             0       -1.151456   -0.758236    0.000000
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.661877   -0.007152   -0.008065
    2          8             0       -0.749352    0.121869    0.004128
    3          1             0        1.089211    1.005032   -0.012074
    4          1             0        1.120657   -0.607207   -0.944651
    5          1             0        1.024791   -0.635842    0.986511
    6          1             0       -1.166263   -0.764144    0.000937
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.653337   -0.002279   -0.015079
    2          8             0       -0.748680    0.124814    0.005563
    3          1             0        1.092427    0.986348   -0.026365
    4          1             0        1.171433   -0.607418   -0.928521
    5          1             0        1.059978   -0.692655    1.044461
    6          1             0       -1.164110   -0.793188    0.001510
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.644668    0.005034   -0.015178
    2          8             0       -0.746371    0.129164    0.007638
    3          1             0        1.097400    0.969723   -0.036108
    4          1             0        1.236399   -0.649817   -0.878050
    5          1             0        1.058703   -0.728271    0.971985
    6          1             0       -1.166548   -0.854194    0.001979
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.636754    0.007083   -0.011536
    2          8             0       -0.737631    0.130551    0.007865
    3          1             0        1.118173    0.980886   -0.023055
    4          1             0        1.219632   -0.675141   -0.826612
    5          1             0        1.019863   -0.703152    0.861241
    6          1             0       -1.176817   -0.911393    0.001722
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.629362    0.008507   -0.009365
    2          8             0       -0.725182    0.130248    0.006391
    3          1             0        1.144187    0.996266    0.002869
    4          1             0        1.143242   -0.681006   -0.789064
    5          1             0        0.983190   -0.666546    0.795984
    6          1             0       -1.199555   -0.969881    0.001021
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.632699    0.009839   -0.010568
    2          8             0       -0.718445    0.133442    0.004882
    3          1             0        1.159228    0.989963    0.033644
    4          1             0        1.034908   -0.676097   -0.770038
    5          1             0        0.931290   -0.634693    0.784852
    6          1             0       -1.200821   -1.067511    0.000285
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.637715    0.011607   -0.016945
    2          8             0       -0.712383    0.141125    0.004541
    3          1             0        1.174957    0.978212    0.058508
    4          1             0        0.941912   -0.658517   -0.761808
    5          1             0        0.880236   -0.622673    0.832905
    6          1             0       -1.227510   -1.229088   -0.000094
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.636831    0.013266   -0.018925
    2          8             0       -0.702572    0.152858    0.003444
    3          1             0        1.205596    0.968303    0.076392
    4          1             0        0.857669   -0.644891   -0.814981
    5          1             0        0.841451   -0.604859    0.909228
    6          1             0       -1.278623   -1.457557   -0.000531
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.631948    0.013053   -0.013455
    2          8             0       -0.692450    0.169056    0.001769
    3          1             0        1.248836    0.953694    0.084513
    4          1             0        0.804900   -0.639591   -0.913268
    5          1             0        0.818118   -0.567121    0.961473
    6          1             0       -1.346403   -1.741439   -0.001004
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.626102    0.007546   -0.003203
    2          8             0       -0.684113    0.188568   -0.000293
    3          1             0        1.292936    0.938452    0.062044
    4          1             0        0.785135   -0.604677   -0.966350
    5          1             0        0.815358   -0.523643    0.948094
    6          1             0       -1.429526   -2.049098   -0.001076
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.620664   -0.001144    0.004978
    2          8             0       -0.678647    0.210841   -0.001435
    3          1             0        1.325776    0.918324    0.015909
    4          1             0        0.803055   -0.543137   -0.947049
    5          1             0        0.827908   -0.497910    0.895399
    6          1             0       -1.514343   -2.366128   -0.000567
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.616538   -0.006903    0.008903
    2          8             0       -0.675048    0.234190   -0.001778
    3          1             0        1.333637    0.898912   -0.032161
    4          1             0        0.850012   -0.497213   -0.907487
    5          1             0        0.863038   -0.511390    0.861941
    6          1             0       -1.611446   -2.680677    0.000161
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.616493   -0.006226    0.011229
    2          8             0       -0.674318    0.246824   -0.002229
    3          1             0        1.323496    0.886571   -0.053128
    4          1             0        0.879932   -0.498427   -0.907645
    5          1             0        0.894583   -0.537763    0.862268
    6          1             0       -1.672887   -2.849004    0.000425
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.619945   -0.004572    0.011135
    2          8             0       -0.675867    0.246942   -0.002444
    3          1             0        1.311150    0.892642   -0.050808
    4          1             0        0.877188   -0.513347   -0.916060
    5          1             0        0.899293   -0.551852    0.872905
    6          1             0       -1.678706   -2.847517    0.000356
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.619946   -0.004572    0.011135
    2          8             0       -0.675868    0.246942   -0.002444
    3          1             0        1.311147    0.892645   -0.050807
    4          1             0        0.877189   -0.513352   -0.916065
    5          1             0        0.899294   -0.551853    0.872906
    6          1             0       -1.678708   -2.847517    0.000356
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.619946   -0.004572    0.011135
    2          8             0       -0.675868    0.246942   -0.002444
    3          1             0        1.311147    0.892645   -0.050807
    4          1             0        0.877189   -0.513352   -0.916065
    5          1             0        0.899294   -0.551853    0.872906
    6          1             0       -1.678708   -2.847517    0.000356
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.619946   -0.004572    0.011135
    2          8             0       -0.675868    0.246942   -0.002444
    3          1             0        1.311147    0.892645   -0.050807
    4          1             0        0.877189   -0.513352   -0.916065
    5          1             0        0.899294   -0.551853    0.872906
    6          1             0       -1.678708   -2.847517    0.000356
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.619946   -0.004572    0.011135
    2          8             0       -0.675868    0.246942   -0.002444
    3          1             0        1.311147    0.892645   -0.050807
    4          1             0        0.877189   -0.513352   -0.916065
    5          1             0        0.899294   -0.551853    0.872906
    6          1             0       -1.678708   -2.847517    0.000356
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.619946   -0.004572    0.011135
    2          8             0       -0.675868    0.246942   -0.002444
    3          1             0        1.311147    0.892645   -0.050807
    4          1             0        0.877189   -0.513352   -0.916065
    5          1             0        0.899294   -0.551853    0.872906
    6          1             0       -1.678708   -2.847517    0.000356
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.619946   -0.004572    0.011135
    2          8             0       -0.675868    0.246942   -0.002444
    3          1             0        1.311147    0.892645   -0.050807
    4          1             0        0.877189   -0.513352   -0.916065
    5          1             0        0.899294   -0.551853    0.872906
    6          1             0       -1.678708   -2.847517    0.000356
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.619946   -0.004572    0.011135
    2          8             0       -0.675868    0.246942   -0.002444
    3          1             0        1.311147    0.892645   -0.050807
    4          1             0        0.877189   -0.513352   -0.916065
    5          1             0        0.899294   -0.551853    0.872906
    6          1             0       -1.678708   -2.847517    0.000356
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.619946   -0.004572    0.011135
    2          8             0       -0.675868    0.246942   -0.002444
    3          1             0        1.311147    0.892645   -0.050807
    4          1             0        0.877189   -0.513352   -0.916065
    5          1             0        0.899294   -0.551853    0.872906
    6          1             0       -1.678708   -2.847517    0.000356
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.619946   -0.004572    0.011135
    2          8             0       -0.675868    0.246942   -0.002444
    3          1             0        1.311147    0.892645   -0.050807
    4          1             0        0.877189   -0.513352   -0.916065
    5          1             0        0.899294   -0.551853    0.872906
    6          1             0       -1.678708   -2.847517    0.000356
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.619946   -0.004572    0.011135
    2          8             0       -0.675868    0.246942   -0.002444
    3          1             0        1.311147    0.892645   -0.050807
    4          1             0        0.877189   -0.513352   -0.916065
    5          1             0        0.899294   -0.551853    0.872906
    6          1             0       -1.678708   -2.847517    0.000356
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.619946   -0.004572    0.011135
    2          8             0       -0.675868    0.246942   -0.002444
    3          1             0        1.311147    0.892645   -0.050807
    4          1             0        0.877189   -0.513352   -0.916065
    5          1             0        0.899294   -0.551853    0.872906
    6          1             0       -1.678708   -2.847517    0.000356
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.619946   -0.004572    0.011135
    2          8             0       -0.675868    0.246942   -0.002444
    3          1             0        1.311147    0.892645   -0.050807
    4          1             0        0.877189   -0.513352   -0.916065
    5          1             0        0.899294   -0.551853    0.872906
    6          1             0       -1.678708   -2.847517    0.000356
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.619946   -0.004572    0.011135
    2          8             0       -0.675868    0.246942   -0.002444
    3          1             0        1.311147    0.892645   -0.050807
    4          1             0        0.877189   -0.513352   -0.916065
    5          1             0        0.899294   -0.551853    0.872906
    6          1             0       -1.678708   -2.847517    0.000356
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.619946   -0.004572    0.011135
    2          8             0       -0.675868    0.246942   -0.002444
    3          1             0        1.311147    0.892645   -0.050807
    4          1             0        0.877189   -0.513352   -0.916065
    5          1             0        0.899294   -0.551853    0.872906
    6          1             0       -1.678708   -2.847517    0.000356
 ---------------------------------------------------------------------

 Entering Gaussian System  

                          Input orientation:   
 ---------------------------------------------------------------------
 Center     Atomic     Atomic              Coordinates (Angstroms)
 Number     Number      Type              X           Y           Z 
 ---------------------------------------------------------------------
    1          6             0        0.619946   -0.004572    0.011135
    2          8             0       -0.675868    0.246942   -0.002444
    3          1             0        1.311147    0.892645   -0.050807
    4          1             0        0.877189   -0.513352   -0.916065
    5          1             0        0.899294   -0.551853    0.872906
    6          1             0       -1.678708   -2.847517    0.000356
 ---------------------------------------------------------------------
